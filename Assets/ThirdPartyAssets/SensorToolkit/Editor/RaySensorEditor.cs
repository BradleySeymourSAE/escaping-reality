﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;


namespace SensorToolkit
{
    [CustomEditor(typeof(RaySensor))]
    [CanEditMultipleObjects]
    public class RaySensorEditor : Editor
    {
        SerializedProperty length;
        SerializedProperty radius;
        SerializedProperty ignoreList;
        SerializedProperty tagFilterEnabled;
        SerializedProperty tagFilter;
        SerializedProperty obstructedByLayers;
        SerializedProperty detectsOnLayers;
        SerializedProperty detectionMode;
        SerializedProperty direction;
        SerializedProperty worldSpace;
        SerializedProperty sensorUpdateMode;
        SerializedProperty initialBufferSize;
        SerializedProperty dynamicallyIncreaseBufferSize;
        SerializedProperty onDetected;
        SerializedProperty onLostDetection;
        SerializedProperty onObstructed;
        SerializedProperty onClear;

        RaySensor raySensor;
        bool isTesting = false;
        bool showEvents = false;

        void OnEnable()
        {
            if (serializedObject == null) return;

            raySensor = serializedObject.targetObject as RaySensor;
            length = serializedObject.FindProperty("Length");
            radius = serializedObject.FindProperty("Radius");
            ignoreList = serializedObject.FindProperty("IgnoreList");
            tagFilterEnabled = serializedObject.FindProperty("EnableTagFilter");
            tagFilter = serializedObject.FindProperty("AllowedTags");
            obstructedByLayers = serializedObject.FindProperty("ObstructedByLayers");
            detectsOnLayers = serializedObject.FindProperty("DetectsOnLayers");
            detectionMode = serializedObject.FindProperty("DetectionMode");
            direction = serializedObject.FindProperty("Direction");
            worldSpace = serializedObject.FindProperty("WorldSpace");
            sensorUpdateMode = serializedObject.FindProperty("SensorUpdateMode");
            initialBufferSize = serializedObject.FindProperty("InitialBufferSize");
            dynamicallyIncreaseBufferSize = serializedObject.FindProperty("DynamicallyIncreaseBufferSize");
            onDetected = serializedObject.FindProperty("OnDetected");
            onLostDetection = serializedObject.FindProperty("OnLostDetection");
            onObstructed = serializedObject.FindProperty("OnObstruction");
            onClear = serializedObject.FindProperty("OnClear");
            raySensor.OnSensorUpdate += onSensorUpdate;
        }

        void OnDisable()
        {
            raySensor.OnSensorUpdate -= onSensorUpdate;
            StopTesting();
        }

        public override void OnInspectorGUI()
        {
            if (raySensor.transform.hasChanged)
            {
                StopTesting();
                raySensor.transform.hasChanged = false;
            }

            serializedObject.Update();
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(length);
            EditorGUILayout.PropertyField(radius);
            EditorGUILayout.PropertyField(direction);
            EditorGUILayout.PropertyField(worldSpace);

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(ignoreList, true);
            tagFilterEditor();
            EditorGUILayout.PropertyField(obstructedByLayers);
            EditorGUILayout.PropertyField(detectsOnLayers);
            EditorGUILayout.PropertyField(detectionMode);
            EditorGUILayout.PropertyField(sensorUpdateMode);

            EditorGUILayout.Space();

            if (showEvents = EditorGUILayout.Foldout(showEvents, "Events")) {
                EditorGUILayout.PropertyField(onDetected);
                EditorGUILayout.PropertyField(onLostDetection);
                EditorGUILayout.PropertyField(onObstructed);
                EditorGUILayout.PropertyField(onClear);
            }

            EditorGUILayout.Space();

            EditorGUILayout.PropertyField(initialBufferSize);
            EditorGUILayout.PropertyField(dynamicallyIncreaseBufferSize);
            if (raySensor.CurrentBufferSize != 0 && raySensor.CurrentBufferSize != raySensor.InitialBufferSize) {
                EditorGUILayout.HelpBox("Buffer size expanded to: " + raySensor.CurrentBufferSize, MessageType.Info);
            }

            if (EditorGUI.EndChangeCheck()) {
                StopTesting();
            }

            EditorGUILayout.Space();

            if (!isTesting && !Application.isPlaying)
            {
                if (GUILayout.Button("TestRay", GUILayout.Width(100)))
                {
                    StartTesting();
                }
            }

            if (EditorApplication.isPlaying || EditorApplication.isPaused || isTesting)
            {
                displayDetectedObjects();
            }

            serializedObject.ApplyModifiedProperties();
        }

        void tagFilterEditor()
        {
            EditorGUILayout.PropertyField(tagFilterEnabled);
            if (tagFilterEnabled.boolValue)
            {
                EditorGUILayout.PropertyField(tagFilter, true);
            }
        }

        void displayDetectedObjects()
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("*** Objects Detected ***");
            foreach (GameObject go in raySensor.DetectedObjects)
            {
                EditorGUILayout.ObjectField(go, typeof(GameObject), true);
            }

            if (!raySensor.IsObstructed) return;

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("*** Ray is Obstructed ***");
            EditorGUILayout.ObjectField(raySensor.ObstructedBy.gameObject, typeof(GameObject), true);
        }

        void onSensorUpdate()
        {
            Repaint();
        }

        void StartTesting()
        {
            if (isTesting || Application.isPlaying || raySensor == null) return;

            isTesting = true;
            raySensor.SendMessage("TestRay");
            SceneView.RepaintAll();
        }

        void StopTesting()
        {
            if (!isTesting || Application.isPlaying || raySensor == null) return;

            isTesting = false;
            raySensor.SendMessage("reset");
            SceneView.RepaintAll();
        }
    }
}