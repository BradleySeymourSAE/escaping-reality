﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CMF
{
	//This character movement input class is an example of how to get input from a keyboard to control the character;
    public class CharacterKeyboardInput : CharacterInput
    {
		public string horizontalInputAxis = "Horizontal";
		public string verticalInputAxis = "Vertical";
		public KeyCode jumpKey = KeyCode.Space;
		public KeyCode cursorDebugKey = KeyCode.I;

		//If this is enabled, Unity's internal input smoothing is bypassed;
		public bool useRawInput = true;
	
		public bool shouldLock = true;

		private void Start()
		{
			if (shouldLock)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
		}

		private void Update()
		{
			if (Input.GetKeyDown(cursorDebugKey) && shouldLock)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
				shouldLock = false;
			}
			else if (Input.GetKeyDown(cursorDebugKey) && !shouldLock)
			{
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
				shouldLock = true;
			}
		}

		public override float GetHorizontalMovementInput()
		{
			if(useRawInput)
				return Input.GetAxisRaw(horizontalInputAxis);
			else
				return Input.GetAxis(horizontalInputAxis);
		}

		public override float GetVerticalMovementInput()
		{
			if(useRawInput)
				return Input.GetAxisRaw(verticalInputAxis);
			else
				return Input.GetAxis(verticalInputAxis);
		}

		public override bool IsJumpKeyPressed()
		{
			return Input.GetKey(jumpKey);
		}
    }
}
