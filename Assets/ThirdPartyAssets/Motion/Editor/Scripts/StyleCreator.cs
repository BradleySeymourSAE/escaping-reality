﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

namespace Michsky.UI.MTP
{
    public class StyleCreator : EditorWindow
    {
        static StyleCreator window;
        protected GUIStyle panelStyle;
        protected GUIStyle lipStyle;
        protected GUIStyle lipAltStyle;

        Vector2 scrollPosition = Vector2.zero;

        public Texture2D bannerTexture;
        public StyleCreatorList styleCreatorList;
        StyleVideoPreview tempWindow;

        [MenuItem("Tools/Motion Titles Pack/Style Creator", false, 0)]
        public static void ShowWindow()
        {
            window = GetWindow<StyleCreator>("MTP - Style Creator");
            window.minSize = new Vector2(560, 534);
            window.maxSize = new Vector2(560, 534);
        }

        void OnEnable()
        {
            try
            {
                if (bannerTexture == null)
                    bannerTexture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/" + EditorPrefs.GetString("MTP.StyleCreator.RootFolder") + "Banner.png", typeof(Texture2D));

                if (styleCreatorList == null)
                    styleCreatorList = (StyleCreatorList)AssetDatabase.LoadAssetAtPath("Assets/" + EditorPrefs.GetString("MTP.StyleCreator.RootFolder") + "StyleData.asset", typeof(StyleCreatorList));
            }

            catch
            {
                if (EditorUtility.DisplayDialog("Motion Titles Pack", "Cannot open the creator due to missing/incorrect root folder. " +
                 "You can change the root folder by clicking 'Fix' button and enabling 'Change Root Folder'.", "Fix", "Cancel"))
                    ShowRootManager();
            }
        }

        void OnDisable()
        {
            if (tempWindow != null)
                tempWindow.Close();
        }

        void OnGUI()
        {
            // Custom skin
            GUISkin customSkin;
            Color defaultColor = GUI.color;
            Color lipBGColor;
            Color lipAltBGColor;

            if (EditorGUIUtility.isProSkin == true)
            {
                customSkin = (GUISkin)Resources.Load("Editor\\Skin\\MTP Skin Dark");
                // lipBGColor = new Color32(30, 35, 40, 255);
                // lipAltBGColor = new Color32(25, 30, 35, 255);
                lipBGColor = new Color32(47, 47, 47, 255);
                lipAltBGColor = new Color32(42, 42, 42, 255);
            }

            else
            {
                customSkin = (GUISkin)Resources.Load("Editor\\Skin\\MTP Skin Light");
                lipBGColor = new Color32(47, 47, 47, 255);
                lipAltBGColor = new Color32(42, 42, 42, 255);
            }

            // Custom panel
            panelStyle = new GUIStyle(GUI.skin.box);
            panelStyle.normal.textColor = GUI.skin.label.normal.textColor;
            panelStyle.margin = new RectOffset(15, 15, 0, 15);
            panelStyle.padding = new RectOffset(0, 0, 0, 0);

            // List item panel
            lipStyle = customSkin.FindStyle("Style Creator LIP");

            // Banner
            GUILayout.BeginHorizontal();
            GUILayout.Label(bannerTexture, GUILayout.Width(1000), GUILayout.Height(90));
            GUILayout.EndHorizontal();

            // Check for style list
            if (styleCreatorList == null)
                return;

            // Scroll panel
            scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, true, GUIStyle.none, GUI.skin.verticalScrollbar, GUILayout.Height(432));
            GUILayout.BeginVertical(panelStyle);

            #region ITEM 1 & 2
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[0].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[0].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);
          
            if (styleCreatorList.styles[0].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[0].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[0].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(0);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(0);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[1].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[1].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[1].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[1].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[1].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(1);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(1);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 3 & 4
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[2].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[2].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[2].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[2].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[2].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(2);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(2);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[3].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[3].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[3].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[3].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[3].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(3);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(3);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 5 & 6
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[4].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[4].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[4].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[4].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[4].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(4);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(4);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[5].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[5].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[5].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[5].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[5].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(5);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(5);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 7 & 8
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[6].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[6].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[6].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[6].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[6].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(6);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(6);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[7].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[7].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[7].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[7].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[7].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(7);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(7);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 9 & 10
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[8].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[8].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[8].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[8].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[8].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(8);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(8);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[9].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[9].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[9].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[9].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[9].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(9);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(9);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 11 & 12
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[10].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[10].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[10].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[10].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[10].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(10);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(10);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[11].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[11].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[11].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[11].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[11].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(11);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(11);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 13 & 14
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[12].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[12].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[12].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[12].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[12].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(12);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(12);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[13].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[13].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[13].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[13].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[13].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(13);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(13);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 15 & 16
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[14].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[14].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[14].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[14].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[14].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(14);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(14);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[15].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[15].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[15].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[15].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[15].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(15);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(15);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 17 & 18
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[16].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[16].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[16].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[16].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[16].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(16);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(16);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[17].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[17].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[17].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[17].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[17].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(17);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(17);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            #region ITEM 19 & 20
            GUI.backgroundColor = lipAltBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[18].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[18].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[18].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[18].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[18].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(18);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(18);

            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUI.backgroundColor = lipBGColor;
            GUILayout.BeginHorizontal(lipStyle);
            GUI.backgroundColor = defaultColor;
            GUILayout.BeginVertical();

            GUILayout.Box(styleCreatorList.styles[19].stylePreview, customSkin.FindStyle("Style Creator Preview"));
            EditorGUILayout.LabelField(styleCreatorList.styles[19].styleTitle, customSkin.FindStyle("Style Creator Title"));

            GUILayout.BeginHorizontal();
            GUILayout.Space(85);

            if (styleCreatorList.styles[19].customContent == true)
                GUILayout.Box(styleCreatorList.ccEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.ccDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[19].customizableWidth == true)
                GUILayout.Box(styleCreatorList.cwEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.cwDisabled, customSkin.FindStyle("Style Creator Indicator"));

            if (styleCreatorList.styles[19].customizableHeight == true)
                GUILayout.Box(styleCreatorList.chEnabled, customSkin.FindStyle("Style Creator Indicator"));
            else
                GUILayout.Box(styleCreatorList.chDisabled, customSkin.FindStyle("Style Creator Indicator"));

            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Play")))
                PlayPreviewVideo(19);

            if (GUILayout.Button("", customSkin.FindStyle("Style Creator Create")))
                CreateObject(19);

            GUILayout.EndHorizontal();
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
            #endregion

            // Scroll Panel End
            GUILayout.EndVertical();
            GUILayout.EndScrollView();

            if (GUI.enabled == true)
                Repaint();
        }

        public void PlayPreviewVideo(int styleIndex)
        {
            StyleVideoPreview videoWindow = (StyleVideoPreview)EditorWindow.GetWindow(typeof(StyleVideoPreview));
            GUIContent titleContent = new GUIContent("MTP Preview: " + styleCreatorList.styles[styleIndex].styleTitle);
            videoWindow.titleContent = titleContent;
            videoWindow.videoClip = styleCreatorList.styles[styleIndex].styleVideoPreview;
            videoWindow.UpdateVideo();
            tempWindow = videoWindow;
        }

        public void CreateObject(int styleIndex)
        {
            try
            {
                GameObject clone = Instantiate(styleCreatorList.styles[styleIndex].stylePrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                clone.name = clone.name.Replace("(Clone)", "").Trim();

                Selection.activeObject = clone;

                // Move to canvas if available
                try
                {
                    var canvas = (Canvas)GameObject.FindObjectsOfType(typeof(Canvas))[0];

                    if (canvas != null)
                        clone.transform.SetParent(canvas.transform, false);
                }

                catch { } // Don't even ask...

                Undo.RegisterCreatedObjectUndo(clone, "Created a MTP object");

                if (Application.isPlaying == false)
                    EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
            }

            catch { Debug.LogError("<b>[MTP Creator]</b> Something went wrong while creating the object. Please contact support and make sure to post the full error."); }
        }

        public void ShowRootManager()
        {
            Selection.activeObject = Resources.Load("Root Manager");

            if (Selection.activeObject == null)
                Debug.Log("<b>[Motion Titles Pack]</b> Cannot find the manager. Make sure you have 'Root Manager' asset in Resources folder.");
        }
    }
}