﻿using UnityEditor;

namespace Michsky.UI.MTP
{
    public class InitMUIP
    {
        [InitializeOnLoad]
        public class InitOnLoad
        {
            static InitOnLoad()
            {
                if (!EditorPrefs.HasKey("MTPv1.Installed"))
                {
                    EditorPrefs.SetInt("MTPv1.Installed", 1);
                    EditorUtility.DisplayDialog("Hello there!", "Thank you for purchasing Motion Titles Pack." +
                        "\r\rMake sure to import/update TextMesh Pro via Package Manager if you haven't already.", "Got it!");
                }

                if (!EditorPrefs.HasKey("MTP.StyleCreator.Upgraded"))
                {
                    EditorPrefs.SetInt("MTP.StyleCreator.Upgraded", 1);
                    EditorPrefs.SetString("MTP.StyleCreator.RootFolder", "Motion Titles Pack/Style Creator/");
                }
            }
        }
    }
}