﻿using UnityEngine;
using UnityEngine.SceneManagement;

[AddComponentMenu("JU TPS/Scene Management/Trigger Load Level")]
public class SimpleLevelTransition : MonoBehaviour
{
    [SerializeField] private string sceneName = "Hub";
    [SerializeField] private LoadSceneMode mode = LoadSceneMode.Single;
    [SerializeField] private bool isAsync;

    public void LoadSceneByName(string value)
	{
        if (isAsync)
            SceneManager.LoadSceneAsync(value, mode);
        else
            SceneManager.LoadScene(value, mode);
	}

   
    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "Player")
        {
            SceneManager.LoadScene(sceneName);
        }
    }
}
