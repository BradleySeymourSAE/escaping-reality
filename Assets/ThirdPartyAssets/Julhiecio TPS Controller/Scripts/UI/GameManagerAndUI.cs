﻿using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("JU TPS/Gameplay/Game/Game Manager")]
public class GameManagerAndUI : MonoBehaviour
{
	
	public ThirdPersonController PlayerCharacter;

	[Header("UI Settings")]
	public bool hideUserInterfaceOnDeath;
	public bool hideUserInterfaceOnF2Pressed;
	public bool showFPSCounter;
	[SerializeField] private Transform fpsCounter;
	public GameObject gameOnScreenDisplay;
	public bool ShowInteractText;
	public Text InteractText;

	[Header("Crosshair Information")]
	public bool ScaleCrosshairWithPrecision;
	public bool HiddenWhenDriving;
	public Image Crosshair;
	[Range(0.01f, 0.5f)]
	public float CrosshairSensibility = 0.15f;
	Vector3 CrosshairStartSize;
	float CurrentSize;

	public Image Scope;

	[Header("Weapon Information")]
	public bool DisplayWeaponInformation;
	public Text WeaponName;
	public Text BulletsCount;

	[Header("Player Stats")]
	public bool DisplayHeatlhBar;
	public Image HealthBar;


	void Start()
	{
		CrosshairStartSize = Crosshair.transform.localScale;
		CurrentSize = CrosshairStartSize.x;
		if (PlayerCharacter == null)
		{
			PlayerCharacter = FindObjectOfType<ThirdPersonController>();
		}
	}

	void Update()
	{
		if (!PlayerCharacter)
		{
			if (FindObjectOfType<ThirdPersonController>())
			{
				PlayerCharacter = FindObjectOfType<ThirdPersonController>();
			}
			return;
		}

		ControllCrosshair();
		DisplayWeaponInfo();
		DisplayHealth();

		if (PlayerCharacter.IsDead == true && hideUserInterfaceOnDeath == true)
		{
			gameOnScreenDisplay.gameObject.SetActive(false);
		}

        if (Input.GetKeyDown(KeyCode.F2) && hideUserInterfaceOnF2Pressed == true)
        {
			gameOnScreenDisplay.SetActive(!gameOnScreenDisplay.activeInHierarchy);
        }

		bool displayCounter = fpsCounter != null && showFPSCounter == true;

		fpsCounter.gameObject.SetActive(displayCounter);
	
		if (ShowInteractText == true)
		{
			if (PlayerCharacter.ToPickupWeapon == true)
			{
				InteractText.text = "Press [F] to pick up weapon";
			}
			if (PlayerCharacter.ToEnterVehicle == true)
			{
				InteractText.text = "Press [F] to drive the vehicle";
			}
			if (PlayerCharacter.ToEnterVehicle == false && PlayerCharacter.ToPickupWeapon == false )
			{
				InteractText.text = "";
			}
		}

	}
	

	public void ControllCrosshair()
    {
		if (ScaleCrosshairWithPrecision == false)
			return;
		CurrentSize = Mathf.Lerp(CurrentSize, CrosshairStartSize.x, 5 * Time.deltaTime);
		Vector3 crosshairsize = new Vector3(CurrentSize, CurrentSize, CurrentSize);
		Crosshair.transform.localScale = crosshairsize;

		if (PlayerCharacter.IsArmed)
		{
			CurrentSize = CurrentSize + 3 * PlayerCharacter.WeaponInUse.ShotErrorProbability * CrosshairSensibility;
			if(PlayerCharacter.IsAiming && PlayerCharacter.WeaponInUse.AimMode == Weapon.WeaponAimMode.Scope && PlayerCharacter.WeaponInUse.ScopeTexture != null)
            {
				Scope.sprite = PlayerCharacter.WeaponInUse.ScopeTexture;
				Scope.gameObject.SetActive(true);
            }
            else
            {
				Scope.gameObject.SetActive(false);
			}
        }
        else
        {
			Scope.gameObject.SetActive(false);
		}
		//Hidden crosshair when driving
		if (HiddenWhenDriving && PlayerCharacter.IsAiming == false)
		{
			Crosshair.gameObject.SetActive(!PlayerCharacter.IsDriving);
		}else if (hideUserInterfaceOnDeath)
        {
			Crosshair.gameObject.SetActive(false);
		}
	}
	public void DisplayWeaponInfo()
    {
		if (DisplayWeaponInformation == false)
			return;
		if (PlayerCharacter.WeaponInUse != null && DisplayWeaponInformation)
		{
			if (PlayerCharacter.WeaponInUse.TotalBullets > 0 || PlayerCharacter.WeaponInUse.BulletsAmounts > 0)
			{
				BulletsCount.text = PlayerCharacter.WeaponInUse.BulletsAmounts + "/" + PlayerCharacter.WeaponInUse.TotalBullets;
				BulletsCount.color = Color.white;
			}
			if (PlayerCharacter.WeaponInUse.TotalBullets <= 0 && PlayerCharacter.WeaponInUse.BulletsAmounts <= 0)
			{
				BulletsCount.text = "No ammo";
				BulletsCount.color = Color.red;
			}
			WeaponName.text = PlayerCharacter.WeaponInUse.WeaponName;
		}
		else
		{
			BulletsCount.text = "";
			WeaponName.text = "";
		}
	}
	public void DisplayHealth()
    {
		if (DisplayHeatlhBar == false)
			return;
		//LifeBar
		HealthBar.fillAmount = Mathf.Lerp(HealthBar.fillAmount, PlayerCharacter.Health / 100, 10 * Time.deltaTime);
	}
}
