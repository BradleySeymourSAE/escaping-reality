﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JU_INPUT_SYSTEM;
[AddComponentMenu("JU TPS/Gameplay/Game/JU Input System")]

public class JUInputSystem : MonoBehaviour
{
	GameManagerAndUI Game;

	//Move and Rotate Axis
	[HideInInspector] public float MoveHorizontal;
	[HideInInspector] public float MoveVertical;
	[HideInInspector] public float RotateHorizontal;
	[HideInInspector] public float RotateVertical;


	//Input Bools
	[HideInInspector]public bool PressedShooting, PressedAiming, PressedReload, PressedRun, PressedJump,
		 PressedCrouch, PressedRoll, PressedPickupWeapon, PressedEnterVehicle, PressedNextWeapon, PressedPreviousWeapon;

	[HideInInspector]public bool PressedShootingDown, PressedAimingDown, PressedReloadDown, PressedRunDown, PressedJumpDown,
		 PressedCrouchDown, PressedRollDown, PressedPickupWeaponDown, PressedEnterVehicleDown, PressedNextWeaponDown, PressedPreviousWeaponDown;
	
	[HideInInspector]public bool PressedShootingUp, PressedAimingUp, PressedReloadUp, PressedRunUp, PressedJumpUp,
		 PressedCrouchUp, PressedRollUp, PressedPickupWeaponUp, PressedEnterVehicleUp, PressedNextWeaponUp, PressedPreviousWeaponUp;


	private void Awake()
	{
		Game = FindObjectOfType<GameManagerAndUI>();
	}
    private void Update()
    {
		if (Game && Game.PlayerCharacter != null)
		{
			UpdateGetButtonDown();
			UpdateGetButton();
			UpdateGetButtonUp();
			UpdateAxis();
		}
    }


    private void UpdateAxis() 
	{
		// >>> Joystick Movements
		MoveHorizontal = Input.GetAxis("Horizontal");
		MoveVertical = Input.GetAxis("Vertical");

		MoveHorizontal = Mathf.Clamp(MoveHorizontal, -1,1);
		MoveVertical = Mathf.Clamp(MoveVertical, -1, 1);
		
		RotateHorizontal = Input.GetAxis("Mouse X");
		RotateVertical = Input.GetAxis("Mouse Y");

		//Crouch
		if (Input.GetAxis("Crouch") < -0.9 && PressedCrouch == false)
		{
			PressedCrouch = true;
        }
        else
        {
			PressedCrouch = false;
        }
		if (Input.GetAxis("Crouch") > 0.9 && PressedCrouch == false)
		{
			PressedCrouchUp = true;
        }
        else
        {
			PressedCrouchUp = false;
        }

	}

    private void UpdateGetButtonDown()
	{
		// >>> Get Button Down 
			if (Input.GetButtonDown("Fire1"))
			{
				PressedShootingDown = true;
			}
			else
			{
				PressedShootingDown = false;
			}

			if (Input.GetButtonDown("Fire2"))
			{
				PressedAimingDown = true;
			}
			else
			{
				PressedAimingDown = false;
			}

		if (Input.GetButtonDown("Reload"))
		{
			PressedJumpDown = true;
		}
		else
		{
			PressedJumpDown = false;
		}

		if (Input.GetButtonDown("Jump"))
		{
			PressedJumpDown = true;
		}
		else
		{
			PressedJumpDown = false;
		}

		if (Input.GetButtonDown("Run"))
		{
			PressedRunDown = true;
		}
		else
		{
			PressedRunDown = false;
		}

		if (Input.GetButtonDown("Roll"))
		{
			PressedRollDown = true;
		}
		else
		{
			PressedRollDown = false;
		}

		if (Input.GetButtonDown("Crouch"))
		{
			PressedCrouchDown = true;
		}
		else
		{
			PressedCrouchDown = false;
		}

		if (Input.GetButtonDown("Reload"))
		{
			PressedReloadDown = true;
		}
		else
		{
			PressedReloadDown = false;
		}

		if (Input.GetButtonDown("Interact"))
		{
			PressedPickupWeaponDown = true;
		}
		else
		{
			PressedPickupWeaponDown = false;
		}

		if (Input.GetButtonDown("Interact"))
		{
			PressedEnterVehicleDown = true;
		}
		else
		{
			PressedEnterVehicleDown = false;
		}

		if (Input.GetButtonDown("Next"))
		{
			PressedNextWeaponDown = true;
		}
		else
		{
			PressedNextWeaponDown = false;
		}

		if (Input.GetButtonDown("Previous"))
		{
			PressedPreviousWeaponDown = true;
		}
		else
		{
			PressedPreviousWeaponDown = false;
		}
	}
	private void UpdateGetButton()
    {

			if (Input.GetButton("Fire1"))
			{
				PressedShooting = true;
			}
			else
			{
				PressedShooting = false;
			}

			if (Input.GetButton("Fire2"))
			{
				PressedAiming = true;
			}
			else
			{
				PressedAiming = false;
			}

		if (Input.GetButton("Reload"))
		{
			PressedJump = true;
		}
		else
		{
			PressedJump = false;
		}

		if (Input.GetButton("Jump"))
		{
			PressedJump = true;
		}
		else
		{
			PressedJump = false;
		}

		if (Input.GetButton("Run"))
		{
			PressedRun = true;
		}
		else
		{
			PressedRun = false;
		}

		if (Input.GetButton("Roll"))
		{
			PressedRoll = true;
		}
		else
		{
			PressedRoll = false;
		}

		if (Input.GetButton("Crouch"))
		{
			PressedCrouch = true;
		}
		else
		{
			PressedCrouch = false;
		}

		if (Input.GetButton("Reload"))
		{
			PressedReload = true;
		}
		else
		{
			PressedReload = false;
		}

		if (Input.GetButton("Interact"))
		{
			PressedPickupWeapon = true;
		}
		else
		{
			PressedPickupWeapon = false;
		}

		if (Input.GetButton("Interact"))
		{
			PressedEnterVehicle = true;
		}
		else
		{
			PressedEnterVehicle = false;
		}

		if (Input.GetButton("Next"))
		{
			PressedNextWeapon = true;
		}
		else
		{
			PressedNextWeapon = false;
		}
		if (Input.GetButton("Previous"))
		{
			PressedPreviousWeapon = true;
		}
		else
		{
			PressedPreviousWeapon = false;
		}
	}
	private void UpdateGetButtonUp()
    {
			if (Input.GetButtonUp("Fire1"))
			{
				PressedShootingUp = true;
			}
			else
			{
				PressedShootingUp = false;
			}

			if (Input.GetButtonUp("Fire2"))
			{
				PressedAimingUp = true;
			}
			else
			{
				PressedAimingUp = false;
			}

		if (Input.GetButtonUp("Reload"))
		{
			PressedJumpUp = true;
		}
		else
		{
			PressedJumpUp = false;
		}

		if (Input.GetButtonUp("Jump"))
		{
			PressedJumpUp = true;
		}
		else
		{
			PressedJumpUp = false;
		}

		if (Input.GetButtonUp("Run"))
		{
			PressedRunUp = true;
		}
		else
		{
			PressedRunUp = false;
		}

		if (Input.GetButtonUp("Roll"))
		{
			PressedRollUp = true;
		}
		else
		{
			PressedRollUp = false;
		}

		if (Input.GetButtonUp("Crouch"))
		{
			PressedCrouchUp = true;
		}
		else
		{
			PressedCrouchUp = false;
		}

		if (Input.GetButtonUp("Reload"))
		{
			PressedReloadUp = true;
		}
		else
		{
			PressedReloadUp = false;
		}

		if (Input.GetButtonUp("Interact"))
		{
			PressedPickupWeaponUp = true;
		}
		else
		{
			PressedPickupWeaponUp = false;
		}

		if (Input.GetButtonUp("Interact"))
		{
			PressedEnterVehicleUp = true;
		}
		else
		{
			PressedEnterVehicleUp = false;
		}

		if (Input.GetButtonUp("Next"))
		{
			PressedNextWeaponUp = true;
		}
		else
		{
			PressedNextWeaponUp = false;
		}
		if (Input.GetButtonUp("Previous"))
		{
			PressedPreviousWeaponUp = true;
		}
		else
		{
			PressedPreviousWeaponUp = false;
		}
	}
}


namespace JU_INPUT_SYSTEM{
	public class JUInput
	{
		static JUInputSystem InputButtons;
		static void GetJUInputInstance()
		{
			if (InputButtons != null) return;
			InputButtons = GameObject.FindObjectOfType<JUInputSystem>();
		}
		public enum Axis { MoveHorizontal, MoveVertical, RotateHorizontal, RotateVertical} 
		public enum Buttons{ ShotButton, AmingButton, JumpButton, RunButton,
							   RollButton, CrouchButton, ReloadButton,
							   PickupButton, EnterVehicleButton, PreviousWeaponButton, NextWeaponButton }

		public static float GetAxis(Axis axis)
		{
			GetJUInputInstance();
			switch (axis)
			{
				case Axis.MoveHorizontal:
					return InputButtons.MoveHorizontal;

				case Axis.MoveVertical:
					return InputButtons.MoveVertical;

				case Axis.RotateHorizontal:
					return InputButtons.RotateHorizontal;

				case Axis.RotateVertical:
					return InputButtons.RotateVertical;

				default:
					return 0;
			}
		}
		public static bool GetButtonDown(Buttons Button)
        {
			GetJUInputInstance();
            switch(Button){
				case Buttons.ShotButton:
					return InputButtons.PressedShootingDown;

				case Buttons.AmingButton:
					return InputButtons.PressedAimingDown;

				case Buttons.JumpButton:
					return InputButtons.PressedJumpDown;

				case Buttons.RunButton:
					return InputButtons.PressedRunDown;

				case Buttons.RollButton:
					return InputButtons.PressedRollDown;

				case Buttons.CrouchButton:
					return InputButtons.PressedCrouchDown;

				case Buttons.ReloadButton:
					return InputButtons.PressedReloadDown;

				case Buttons.PickupButton:
					return InputButtons.PressedPickupWeaponDown;

				case Buttons.EnterVehicleButton:
					return InputButtons.PressedEnterVehicleDown;

				case Buttons.PreviousWeaponButton:
					return InputButtons.PressedPreviousWeaponDown;

				case Buttons.NextWeaponButton:
					return InputButtons.PressedNextWeaponDown;


				default:
					return false;

            }
        }
		public static bool GetButton(Buttons Button)
		{
			GetJUInputInstance();
			switch (Button)
			{
				case Buttons.ShotButton:
					return InputButtons.PressedShooting;

				case Buttons.AmingButton:
					return InputButtons.PressedAiming;

				case Buttons.JumpButton:
					return InputButtons.PressedJump;

				case Buttons.RunButton:
					return InputButtons.PressedRun;

				case Buttons.RollButton:
					return InputButtons.PressedRoll;

				case Buttons.CrouchButton:
					return InputButtons.PressedCrouch;

				case Buttons.ReloadButton:
					return InputButtons.PressedReload;

				case Buttons.PickupButton:
					return InputButtons.PressedPickupWeapon;

				case Buttons.EnterVehicleButton:
					return InputButtons.PressedEnterVehicle;

				case Buttons.PreviousWeaponButton:
					return InputButtons.PressedPreviousWeapon;

				case Buttons.NextWeaponButton:
					return InputButtons.PressedNextWeapon;


				default:
					return false;

			}
		}
		public static bool GetButtonUp(Buttons Button)
		{
			GetJUInputInstance();
			switch (Button)
			{
				case Buttons.ShotButton:
					return InputButtons.PressedShootingUp;

				case Buttons.AmingButton:
					return InputButtons.PressedAimingUp;

				case Buttons.JumpButton:
					return InputButtons.PressedJumpUp;

				case Buttons.RunButton:
					return InputButtons.PressedRunUp;

				case Buttons.RollButton:
					return InputButtons.PressedRollUp;

				case Buttons.CrouchButton:
					return InputButtons.PressedCrouchUp;

				case Buttons.ReloadButton:
					return InputButtons.PressedReloadUp;

				case Buttons.PickupButton:
					return InputButtons.PressedPickupWeaponUp;

				case Buttons.EnterVehicleButton:
					return InputButtons.PressedEnterVehicleUp;

				case Buttons.PreviousWeaponButton:
					return InputButtons.PressedPreviousWeaponUp;

				case Buttons.NextWeaponButton:
					return InputButtons.PressedNextWeaponUp;


				default:
					return false;

			}
		}


	}




}
