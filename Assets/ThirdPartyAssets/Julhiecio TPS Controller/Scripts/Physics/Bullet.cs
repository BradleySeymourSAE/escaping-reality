﻿using UnityEngine;



[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour
{
	private Rigidbody rb;
	[Header("Bullet Settings")]
	public float BulletVelocity;
	public GameObject DestroyBulletParticle;
	public GameObject BulletHole;
	[HideInInspector]
	public Vector3 DestroyBulletRotation;

	[Header("Physics: It is calculated by physics")]
	[Header("Calculated: Follow a path between two points")]
	[Header("Teleport: It is teleported to the hit point.")]
	public BulletMovementType MovementType;

	[SerializeField] protected TagCollection m_AllowedCollisionTags;
	[HideInInspector] public Vector3 FinalPoint;  // the raycasted hit position

	private void Start()
    {
		rb = GetComponent<Rigidbody> ();
		rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
		if (!m_AllowedCollisionTags.Includes("Player") || !m_AllowedCollisionTags.Includes("Bullet"))
		{
			m_AllowedCollisionTags.Add("Player");
			m_AllowedCollisionTags.Add("Bullet");
		}

		//If movement is physic type
		if (MovementType == BulletMovementType.Physics) {
			rb.velocity = transform.forward * BulletVelocity;
        }
        else
        {
			rb.useGravity = false;
        }
		//If movement is teleport type
		if (MovementType == BulletMovementType.Teleport && FinalPoint != Vector3.zero) {
			transform.position = FinalPoint;
		}
    }
	void FixedUpdate(){
		//If movement is calculated type
		if (MovementType == BulletMovementType.Calculated && FinalPoint != Vector3.zero) {
			transform.position = Vector3.MoveTowards (transform.position, FinalPoint, BulletVelocity * Time.deltaTime);
		} 
		if(FinalPoint == Vector3.zero) {
			transform.Translate (0,0, BulletVelocity * Time.deltaTime);
		}
		if(transform.position == FinalPoint && IsInvoking("DestroyBullet") == false){
			Invoke("DestroyBullet", 0.2f);
			rb.velocity = transform.forward * BulletVelocity;
		}
	}
	void OnCollisionEnter(Collision collision)
	{
		if (m_AllowedCollisionTags.Includes(collision.collider.gameObject.tag))
		{ 
			if (FinalPoint == Vector3.zero)
				FinalPoint = transform.position;
		
			var otherObject = collision.collider.gameObject.GetComponent<HealthSystem>();
			if (otherObject != null && otherObject.onDamageReceived != null)
			{
				otherObject.onDamageReceived?.Invoke(10.0f);
			}

			var l_DefaultCollisionParticle = Instantiate(DestroyBulletParticle, FinalPoint, Quaternion.FromToRotation(transform.forward, DestroyBulletRotation) * transform.rotation) as GameObject;
			Destroy(l_DefaultCollisionParticle, 2f);

			if (MovementType != BulletMovementType.Physics)
			{
				var bullethole = Instantiate(BulletHole, FinalPoint, Quaternion.FromToRotation(transform.up, DestroyBulletRotation) * transform.rotation) as GameObject;
				bullethole.transform.position = bullethole.transform.position + bullethole.transform.up * 0.001f;
				bullethole.transform.SetParent(collision.collider.gameObject.transform);
				Destroy(bullethole, 10f);
			}
			DestroyBullet();
		}
	}

	public void DestroyBullet()
	{
		Destroy(gameObject);
	}
	public enum BulletMovementType{
		Physics,
		Calculated,
		Teleport,
	}
}
