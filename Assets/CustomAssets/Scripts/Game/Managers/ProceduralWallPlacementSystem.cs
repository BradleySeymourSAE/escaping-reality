using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;


public class ProceduralWallPlacementSystem : SerializedMonoBehaviour
{
   [SerializeField] private Renderer m_TargetRenderer;
   [SerializeField] private Camera m_CameraReference;
   [SerializeField] private GameObject[] m_WallPrefabs;
   [SerializeField] private Dictionary<int, Transform> spawnedWallsDictionary = new Dictionary<int, Transform>();
   [SerializeField] private int spawnWallAmount = 25;
   [SerializeField] private float m_OffsetXZ = 5.0f;
   [SerializeField] private Vector2 m_WallSizeThreshold = Vector3.one;
   [SerializeField] private TagCollection m_IgnoreTagCollection;

   [SerializeField] [ReadOnly] private Vector2 spawnRange; 
   [SerializeField] [ReadOnly] private Transform m_Container;

 
	#region Getters / Setters 

	public Dictionary<int, Transform> SpawnedWalls
	{
		get => spawnedWallsDictionary;
		private set => spawnedWallsDictionary = value;
	}

	#endregion

	#region Unity References 

	private void Awake()
	{
		if (SpawnedWalls != null)
			SpawnedWalls.Clear();

		if (!m_CameraReference) m_CameraReference = Utility.Find<Camera>(gameObject, true);
		if (m_TargetRenderer != null)
		{
			var bounds = m_TargetRenderer.GetComponent<Collider>().bounds;
			var maximumX = bounds.extents.x;
			var maximumZ = bounds.extents.z;
			spawnRange = new Vector2
			{
				x = maximumX - m_OffsetXZ,
				y = maximumZ - m_OffsetXZ
			};
		}
	}

	private void Start()
	{
		if (!m_Container) m_Container = new GameObject("wall container").transform;
		var level = GameObject.Find("level").transform;
		if (level) m_Container.SetParent(level);

		SpawnProceduralWalls(spawnWallAmount);
	}

	#endregion

	#region private Methods 

	private void SpawnProceduralWalls(int amount)
	{
		if (m_WallPrefabs != null && m_WallPrefabs.Length > 0)
		{ 
			for (int i = 0; i < amount; i++)
			{
				Vector3 desiredPosition = new Vector3
				{
					x = Random.Range(-spawnRange.x, spawnRange.x),
					y = 0,
					z = Random.Range(-spawnRange.y, spawnRange.y)
				};
				Vector3 randomWallSize = new Vector3(
					m_WallSizeThreshold.x,
					1, 
					Random.Range(2.5f, m_WallSizeThreshold.y)
				);

				m_CameraReference.transform.position = desiredPosition; // set camera position to desired position 

				Vector3 origin = m_CameraReference.transform.position; // get the origin of the new position 
				Vector3 direction = m_CameraReference.transform.forward; // get the direction we are facing 
				RaycastHit hit; // store local var for raycast hit information 
				Ray ray = new Ray(origin, direction); // get the camera's origin and facing direction 

				if (Physics.Raycast(ray, out hit, 1000f)) // shoot a ray out and see if we hit the ground 
				{
					if (m_IgnoreTagCollection.Includes(hit.collider.gameObject.tag))
					{
						return;
					}

					var point = hit.point;
					var normal = hit.normal;
					var randomWallPrefab = Utility.RandomElement(m_WallPrefabs);

					float rotateAngleY = Utility.GetAngleFromDirection(Vector3.up); 
		
					Vector3 angle = new Vector3(
						0, 
						Random.Range(-rotateAngleY, rotateAngleY), 
						0
					);

					angle += normal;

					var spawnedWall = Instantiate(
						randomWallPrefab,
						point,
						Quaternion.Euler(angle)
					) as GameObject;
					spawnedWall.transform.localScale = randomWallSize;

					if (spawnedWall && m_Container)
					{ 
						spawnedWall.layer = LayerMask.NameToLayer("Walls");
						spawnedWall.transform.SetParent(m_Container);

						SpawnedWalls.Add(i, spawnedWall.transform);
					}
				}
			}
		}
	}
	#endregion
}
