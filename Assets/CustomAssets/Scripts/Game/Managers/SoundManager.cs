﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;



[InlineEditor]
public class SoundManager : MonoBehaviour
{
    private static SoundManager s_Instance;
    #pragma warning disable
    private static bool debugging = true;

    public static SoundManager instance
    {
        get
        {
            if (s_Instance == null)
                s_Instance = FindObjectOfType<SoundManager>();

            return s_Instance;
        }
    }

	[HorizontalGroup("Audio Source")] 
    [SerializeField]
    private AudioSource defaultAudioSource;

    [TabGroup("UI")]
    [AssetList(Path = "/CustomAssets/Audio/UI SFX", AutoPopulate = true)]
    [InlineEditor] public List<SoundFX> uiSounds;
    
    [TabGroup("Ambient")]
    [AssetList(Path = "/CustomAssets/Audio/Ambient SFX", AutoPopulate = true)]
    [InlineEditor] public List<SoundFX> ambientSounds;
    
    [TabGroup("Weapons")]
    [AssetList(Path = "/CustomAssets/Audio/Weapon SFX", AutoPopulate = true)]
    [InlineEditor] public List<SoundFX> weaponSounds;

    [Button("Play Sound", buttonSize: ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(1.0f, 0.5f, 0.5f, 1f)]
    [ShowIf("debugging")]
    public static void PlaySFX(SoundFX sfx, bool waitToFinish = true, AudioSource audioSource = null)
    {
        if (audioSource == null)
            audioSource = SoundManager.instance.defaultAudioSource;

        if (audioSource == null)
        {
            GameDebug.LogError("SFXManager: You forgot to add a default audio source!");
            return;
        }
        
        if (!waitToFinish)
		{
            audioSource.clip = sfx.sound;
            if (audioSource.clip != null)
			{
                audioSource.pitch = sfx.pitch + Random.Range(-sfx.pitchVariation, sfx.pitchVariation);
                audioSource.volume = sfx.volume + Random.Range(-sfx.volumeVariation, sfx.volumeVariation);
                audioSource.Play();
			}
		}
    }


    public void PlaySound(SoundFX sfx, bool waitToFinish = true, AudioSource audioSource = null)
    {
        if (audioSource == null)
            audioSource = SoundManager.instance.defaultAudioSource;

        if (audioSource == null)
        {
            GameDebug.LogError("SFXManager: You forgot to add a default audio source!");
            return;
        }

        if (!waitToFinish)
        {
            audioSource.clip = sfx.sound;
            if (audioSource.clip != null)
            {
                audioSource.pitch = sfx.pitch + Random.Range(-sfx.pitchVariation, sfx.pitchVariation);
                audioSource.volume = sfx.volume + Random.Range(-sfx.volumeVariation, sfx.volumeVariation);
                audioSource.Play();
            }
        }
    }


    [HorizontalGroup("Audio Source")]
    [ShowIf("@defaultAudioSource == null")]
    [GUIColor(1f,0.5f,0.5f,1f)]
    [Button("Add Audio Source Component")]
    private void AudioStudioEventEmitter()
    {
        defaultAudioSource = this.gameObject.GetComponent<AudioSource>();

        if (defaultAudioSource == null)
            defaultAudioSource = this.gameObject.AddComponent<AudioSource>();
    }

    public enum SFXType
    {
        UI,
        Ambient,
        Weapons
    }
}





