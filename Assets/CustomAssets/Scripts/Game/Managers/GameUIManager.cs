#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Sirenix.OdinInspector;
#endregion


public class GameUIManager : SerializedMonoBehaviour
{
	private static GameUIManager s_Instance;
	#pragma warning disable
	[SerializeField] private bool debugging = false;

	public static GameUIManager instance
	{
		get
		{
			if (s_Instance == null)
			{
				s_Instance = FindObjectOfType<GameUIManager>();
			}
			return s_Instance;
		}
	}

	[ShowIf("debugging")] [EnableIf("debugging")] [SerializeField] private Dictionary<string, GameScreen> screens = new Dictionary<string, GameScreen>();
	[ShowIf("debugging")] [SerializeField] private VoidEvent onGameStarted;
	[ShowIf("debugging")] [SerializeField] private VoidEvent onGameQuit;

	[SerializeField] private WelcomeScreen welcome;
	[SerializeField] private SplashScreen splash;


	protected void Awake() => SetupScreens();

	protected void Update() => UpdateScreens();

	public void OpenWebpage(string url) => Utility.OpenWebpage(url);

	public void SetActiveScreen(string name = "")
	{
		GameScreen screen = null;
		if (screens != null && screens.TryGetValue(name, out screen))
		{
			HideScreens(); // hide all screens 
			EnableScreen(screen); // enable the screen found 
		}
	}

	private void SetupScreens()
	{
		AddScreens();
		foreach (var screen in screens.Values)
		{
			screen.Setup();
		}	
	}

	private void AddScreens()
	{
		screens.Add("welcome", welcome);
		screens.Add("splash", splash);
	}

	private void EnableScreen(GameScreen screen)
	{
		screen.ShowScreen(true);
	}

	private void UpdateScreens()
	{
		if (screens != null && screens.Values.Count > 0)
		{ 
			foreach (var screen in screens.Values)
			{
				screen.UpdateScreen();
			}
		}
	}

	public void HideScreens()
	{
		foreach (KeyValuePair<string, GameScreen> pair in screens)
		{
			var screen = pair.Value;
			screen.ShowScreen(false);
		}
	}
	
	[System.Serializable]
	public class GameScreen
	{
		public GameObject screen;
		public virtual void UpdateScreen()
		{
			if (!IsEnabledGameScreen())
			{
				return;
			}
		}
		public virtual void ShowScreen(bool shouldDisplayScreen) => screen.SetActive(shouldDisplayScreen);
		public virtual void Setup() { }
		public virtual bool IsEnabledGameScreen() => screen.activeSelf == true;
	}

	[System.Serializable]
	public class WelcomeScreen : GameScreen
	{
		[SerializeField] private Image m_Background;
		[SerializeField] private Image m_BackgroundSolid;
		[SerializeField] private TMP_Text m_TopText;
		[SerializeField] private TMP_Text m_BottomText;
		[SerializeField] private Button startButton;
		[SerializeField] private Button quitButton;

		[SerializeField] private Button twitterButton;
		[SerializeField] private Button discordButton;
	
		private bool debugging;
		[ShowIf("debugging")] [SerializeField] private StringEvent onTwitterUrlOpen;
		[ShowIf("debugging")] [SerializeField] private StringEvent onDiscordUrlOpen;

		public override void Setup()
		{
			m_TopText.text = UI.GAME_TITLE_TOP;
			m_BottomText.text = UI.GAME_TITLE_BOTTOM;
			Utility.SetupButtonActions(
				startButton,
				() => GameUIManager.instance.onGameStarted.Raise(),
				UI.START_BUTTON_TEXT
			);
			Utility.SetupButtonActions(
				quitButton,
				() => GameUIManager.instance.onGameQuit.Raise(),
				UI.LEAVE_BUTTON_TEXT
			);
			Utility.SetupButtonActions(discordButton, () => onDiscordUrlOpen.Raise(UI.DISCORD_URL), "DISCORD");
			Utility.SetupButtonActions(twitterButton, () => onTwitterUrlOpen.Raise(UI.TWITTER_URL), "TWITTER");
		}
	}

	[System.Serializable]
	public class SplashScreen : GameScreen
	{
		[SerializeField] private Image background;
		[SerializeField] private TMP_Text m_TopText;
		[SerializeField] private TMP_Text m_BottomText;

		public override void Setup()
		{
			m_TopText.text = UI.GAME_TITLE_TOP;
			m_BottomText.text = UI.GAME_TITLE_BOTTOM;
		}
	}
}

public static class UI
{ 
	public const string DISCORD_URL = "https://discord.gg/FJqv6MACef";
	public const string TWITTER_URL = "https://twitter.com/seemore_gamedev";
	public const string GAME_TITLE_TOP = "ESCAPING", GAME_TITLE_BOTTOM = "REALITY";
	public const string START_BUTTON_TEXT = "START GAME";
	public const string LEAVE_BUTTON_TEXT = "QUIT";

}