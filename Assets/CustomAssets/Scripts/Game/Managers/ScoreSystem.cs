﻿#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CMF;
using Sirenix.OdinInspector;
#endregion


[ExecuteInEditMode]
public class ScoreSystem : MonoBehaviour
{	
	private static ScoreSystem s_Instance;
	public static ScoreSystem instance
	{ 
		get
		{
			if (s_Instance == null)
				s_Instance = FindObjectOfType<ScoreSystem>();

			return s_Instance;
		}
	}

	public bool debug;

	[SerializeField] private ScoreSystemConfigSO m_ScoreSystemConfig;
	public ScoreSystemConfigSO config
	{
		get => m_ScoreSystemConfig;
		private set => m_ScoreSystemConfig = value;
	}

	[SerializeField] [EnableIf("debug")] private bool shouldUpdate;
	[SerializeField] [EnableIf("debug")] private float m_CurrentPlayerScore = 0;
	[SerializeField] [EnableIf("debug")] private float currentScoreTimer;
	[SerializeField] [EnableIf("debug")] private int m_CurrentRoomsCompleted = 0;
	[SerializeField] [EnableIf("debug")] private ThirdPersonController m_CurrentPlayerReference;
	[SerializeField] [EnableIf("debug")] private StringEvent onUpdateScoreTextEvent;



	public int RoomsCompleted
	{
		get => m_CurrentRoomsCompleted;
		private set => m_CurrentRoomsCompleted = value;
	}

	public float PlayerScore
	{
		get => m_CurrentPlayerScore;
		private set => m_CurrentPlayerScore = value;
	}

	private void Start()
	{
		if (!m_CurrentPlayerReference) m_CurrentPlayerReference = FindObjectOfType<ThirdPersonController>();
		m_CurrentPlayerScore = 0;
		currentScoreTimer = 0;
		shouldUpdate = false;
	}

	private void Update()
	{
		if (!shouldUpdate || !m_CurrentPlayerReference)
		{
			var playerReference = FindObjectOfType<ThirdPersonController>();

			if (playerReference != null && playerReference.isActiveAndEnabled)
			{
				m_CurrentPlayerReference = playerReference;
				shouldUpdate = true;
			}
			return;
		}
		currentScoreTimer = 1 * Time.deltaTime;
		ChangeScore(currentScoreTimer);
	}

	public void DespawnEntity(Transform destroyedEntity)
	{
		if (m_CurrentPlayerReference.transform.Equals(destroyedEntity.transform))
		{
			GameDebug.Log("Player died!");
			Reset();
		}
		else
		{
			var entity = destroyedEntity.GetComponent<ZoneOrigin>().GetZoneOrigin;
			float points = 0;
			switch (entity)
			{
				case ZoneOrigin.Zone.RED_ZONE:
					{
						points = config.pointsPerRedSoldier;
					}
					break;
				case ZoneOrigin.Zone.BLUE_ZONE:
					{
						points = config.pointsPerBlueSoldier;
					}
					break;
				case ZoneOrigin.Zone.YELLOW_ZONE:
					{
						points = config.pointsPerYellowSoldier;
					}
					break;
				case ZoneOrigin.Zone.NONE:
					{
						points = 0;
						GameDebug.Log($"Entity: {entity}");
					}
					break;
			}
			ChangeScore(points);
		}
	}

	public void ChangeScore(float amount)
	{
		PlayerScore += amount;

		if (PlayerScore % 100 == 0) // if player has reached 100 points 
		{
			// add bonus points 
			PlayerScore += Random.Range(10.0f, config.bonusPoints);
		}

		var playerScoreText = PlayerScore.ToString("0.0");
		onUpdateScoreTextEvent.Raise(playerScoreText);
	}

	public void LevelCompleted(int increaseLevel)
	{
		RoomsCompleted += increaseLevel; // increase the rooms cleared 

		if (RoomsCompleted >= 3) // if player has cleared 3 rooms 
		{
			// make a sound? 
			ChangeScore(config.bonusPoints); // add bonus points to score 
		}
	}

	public void ResetScore()
	{
		m_CurrentPlayerScore = 0;
		m_CurrentRoomsCompleted = 0;
	}

	public void Reset()
	{
		shouldUpdate = false;
	}
}