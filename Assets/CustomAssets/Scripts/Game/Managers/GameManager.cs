﻿#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#endregion



public class GameManager : MonoBehaviour
{
	private static GameManager s_Instance;
	public static GameManager instance
	{
		get
		{
			if (s_Instance == null)
				s_Instance = FindObjectOfType<GameManager>();

			return s_Instance;
		}
	}

	[SerializeField] private ScoreSystem m_ScoreSystem;
	[SerializeField] private SoundManager m_SoundManager;
	[SerializeField] private GameUIManager m_GameUIManager;

	[SerializeField] private SoundFX loadedBackgroundSound;

	private void Start()
	{
		loadedBackgroundSound.PlaySound();
	}
}