﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;



public class AreaSpawner : SerializedMonoBehaviour
{
    private static readonly Color RedSpawnZoneColor = Color.red;
    private static readonly Color YellowSpawnZoneColor = Color.yellow;
    private static readonly Color BlueSpawnZoneColor = Color.blue;
    private static readonly Color NoSpawnZoneColor = Color.black;
    [SerializeField] private SpawnItemConfigSO m_ZoneSpawnConfig;
    [SerializeField] protected GameObject[] m_SpawnedObjects;
    [SerializeField] private ZoneOrigin m_ZoneOrigin;

    #region Getters / Setters 
    public SpawnItemConfigSO config
	{
        get => m_ZoneSpawnConfig;
        private set => m_ZoneSpawnConfig = value;
	}

	#endregion

    protected Coroutine m_SpawnRoutine;
    protected bool allowSpawning;
    protected float remainingTimeUntilNextSpawn;

	#region Unity References 

	protected virtual void Awake()
    {
        if (!m_ZoneSpawnConfig)
            allowSpawning = false;
        else
            allowSpawning = true;

        if (!m_ZoneOrigin) m_ZoneOrigin = Utility.Assign<ZoneOrigin>(gameObject);

        m_ZoneOrigin = GetComponent<ZoneOrigin>();
        m_SpawnedObjects = new GameObject[config.spawnCount];
    }

    protected virtual void Start()
    {
        if (!allowSpawning || !config)
            return;

        if (config.zoneAreaEffect)
        { 
            var spawnZoneEffect = Instantiate(config.zoneAreaEffect, transform.position, Quaternion.identity) as GameObject;
            spawnZoneEffect.transform.SetParent(transform);
        }

        InitializeSpawning();
    }

	#endregion

	#region Public Methods 

	[Button("Spawn", buttonSize: ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(0, 1.0f, 1.0f, 1)]
	public virtual void InitializeSpawning()
	{
        for (int i = 0; i < config.startingSpawnCount; i++)
        {
            Spawn(); // spawn the starting amount of soldiers into the area 
        }

        if (m_SpawnRoutine != null)
        {
            StopCoroutine(m_SpawnRoutine);
        }
        m_SpawnRoutine = StartCoroutine(SpawnRoutine());
	}

    public virtual int NextAvailableSpawnIndex
    {
        get
        {
            for (int i = 0; i < m_SpawnedObjects.Length; i++)
            {
                if (m_SpawnedObjects[i] == null) return i;
            }
            return -1;
        }
    }

    #endregion

    #region Protected Methods 

    protected virtual IEnumerator SpawnRoutine()
    {
      remainingTimeUntilNextSpawn = config.spawnRate;
      while (true)
      {
        remainingTimeUntilNextSpawn -= Time.deltaTime;
        if (remainingTimeUntilNextSpawn <= 0f)
        {
            Spawn();
        }
        yield return null;
        }
    }

	#endregion

    protected virtual void Spawn()
    {
         remainingTimeUntilNextSpawn = config.spawnRate;
         var l_NextAvailableSpawnSlot = NextAvailableSpawnIndex;
         if (l_NextAvailableSpawnSlot == -1) return; // No spawn slots available

         int l_NumberOfTriesIndex = 0;
         Vector3 l_Position;
     
         do
         {
          l_NumberOfTriesIndex++;
          if (l_NumberOfTriesIndex > 10)
          {
             GameDebug.LogWarning($"Failed to find spawn location after 10 tries, aborting. {gameObject}");
              return;
           }
        
            l_Position = SelectNextLocation();
          } 
          while 
          (
            IsObstructedLocation(l_Position)
          );

          var l_NewInstance = Instantiate(config.prefab, l_Position, transform.rotation) as GameObject;
          if (l_NewInstance && config.zoneSpawnEffect != null)
		  {
              var l_SpawnedEffect = Instantiate(config.zoneSpawnEffect, l_NewInstance.transform.position, Quaternion.identity) as GameObject;
              Destroy(l_SpawnedEffect, 3.0f);
		  }
          m_SpawnedObjects[l_NextAvailableSpawnSlot] = l_NewInstance;
    }

     protected virtual Vector3 SelectNextLocation()
     {
            var l_Dimensions = config.area;
            var l_RandomisedVector = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            var nextLocation = Vector3.Scale(l_Dimensions, l_RandomisedVector) + transform.position;
            return nextLocation;
      }

     protected virtual bool IsObstructedLocation(Vector3 location) => Physics.CheckSphere(location, config.checkSphereRadius, config.obstructingLayers);

     protected virtual void OnDrawGizmosSelected()
     {
        if (!isActiveAndEnabled || !config) return;
        var color = Gizmos.color;
        
        if (m_ZoneOrigin != null)
		{
            ZoneOrigin.Zone origin = m_ZoneOrigin.GetZoneOrigin;

		    switch (origin)
		    {
			    case ZoneOrigin.Zone.RED_ZONE: 
                   color = RedSpawnZoneColor;
                break;
			    case ZoneOrigin.Zone.BLUE_ZONE:
                    color = BlueSpawnZoneColor;
                break;
                case ZoneOrigin.Zone.YELLOW_ZONE:
                    color = YellowSpawnZoneColor;
                break;
                case ZoneOrigin.Zone.NONE:
                    color = NoSpawnZoneColor;
                break;
		    }
        }
        
        Gizmos.color = color;
		Gizmos.DrawWireCube(transform.position, config.area);
        Gizmos.DrawWireSphere(transform.position + Vector3.up * (config.area.y / 2f + config.checkSphereRadius), config.checkSphereRadius);
        }
   }