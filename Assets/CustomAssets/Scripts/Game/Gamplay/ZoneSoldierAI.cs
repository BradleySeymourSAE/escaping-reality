using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SensorToolkit;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif


public class ZoneSoldierAI : MonoBehaviour
{
	public bool showAttributes;
	public bool debug;

	[SerializeField] private Animator m_Animator;
	[SerializeField] private Rigidbody m_Rigidbody;
	[SerializeField] private SteeringRig m_SteeringSensor;
	[SerializeField] private RangeSensor m_ViewRangeSensor;
	[SerializeField] private SoldierWeapon m_SoldierWeapon;

	[ShowIf("showAttributes")]
	[Title("AI", subtitle: "Adjustable soldier AI settings", bold: true, horizontalLine: true, TitleAlignment = TitleAlignments.Centered)]
	[SerializeField] private ZoneSoldierAIAttributes attributes;


	[System.Serializable]
	public class ZoneSoldierAIMovement
	{
		[SerializeField] private ZoneSoldierAI m_CurrentSoldier;

		public void Setup(ZoneSoldierAI p_SoldierReference)
		{
			if (p_SoldierReference != null)
			{
				m_CurrentSoldier = p_SoldierReference;
			}
		}

		public void UpdateSoldierMovement(ZoneSoldierAIAttributes attributes, float FixedTime)
		{
			var l_TurningAngle = Utility.SignedAngleXZ(m_CurrentSoldier.m_Rigidbody.transform.forward, m_CurrentSoldier.FacingDirection);
			var l_RotationTorque = Mathf.Clamp(l_TurningAngle / 10.0f, -1.0f, 1.0f) * attributes.turn;
			var l_ForwardMovement = Vector3.Dot(m_CurrentSoldier.transform.forward, m_CurrentSoldier.MovementVelocity.normalized); // 
			var l_MovementForce = Mathf.Lerp(attributes.strafe, attributes.speed, Mathf.Clamp(l_ForwardMovement, 0f, 1f)) * m_CurrentSoldier.MovementVelocity;

			m_CurrentSoldier.m_Rigidbody.AddTorque(Vector3.up * l_RotationTorque);
			m_CurrentSoldier.m_Animator.SetFloat(attributes.VelocityHash, l_ForwardMovement);
			m_CurrentSoldier.m_Rigidbody.AddForce(l_MovementForce);
		}

	}

	ZoneSoldierAIMovement m_SoldierMovement = new ZoneSoldierAIMovement();
	[TagSelector] public string zone = "BLUE ZONE";

	[ShowIf("debug")] private Vector3 m_Velocity;
	[ShowIf("debug")] private Vector3 m_FacingDirection;
	[ShowIf("debug")] private Transform m_SpawnZoneOrigin;
	Coroutine m_StateRoutine; 
	
	public SoldierWeapon AttachedWeapon
	{
		get => m_SoldierWeapon;
		private set => m_SoldierWeapon = value;
	}

	public Transform Team 
	{
		get
		{
			if (!m_SpawnZoneOrigin)
			{
				m_SpawnZoneOrigin = GameObject.FindGameObjectWithTag(zone).transform;
			}
			return m_SpawnZoneOrigin;
		}
	}

	#region Getters / Setters 

	public Vector3 MovementVelocity 
	{ 
		get => m_Velocity; 
		set => m_Velocity = value.sqrMagnitude > 1.0f ? value.normalized : value;
	}

	public Vector3 FacingDirection
	{
		get => m_FacingDirection;
		set => m_FacingDirection = value.normalized;
	}

	public List<ZoneOrigin> SpottedTargets
	{
		get
		{
			var detectedTargets = m_ViewRangeSensor.GetDetectedByComponent<ZoneOrigin>(); // all targets with the ZoneOrigin component attached. 

			for (int i = detectedTargets.Count - 1; i >= 0; i--) // loop backwards through all the detected targets 
			{
				var l_CurrentTargetExists = detectedTargets[i].GetComponent<ZoneOrigin>();
				if (!l_CurrentTargetExists || !l_CurrentTargetExists.GetZoneOrigin.Equals(ZoneOrigin.Zone.NONE))
				{
					// if the zone origin is not equal to ZoneOrigin.Zone.NONE (Player), remote object from index 
					detectedTargets.RemoveAt(i);
				}
			}
			return detectedTargets;
		}
	}

	public List<ZoneOrigin> SpottedFriendlys
	{
		get
		{
			var detected = m_ViewRangeSensor.GetDetectedByComponent<ZoneOrigin>();
			for (int i = detected.Count - 1; i >= 0; --i)
			{
				var current = detected[i].GetComponent<ZoneOrigin>();
				if (!current || current.GetZoneOrigin.Equals(ZoneOrigin.Zone.NONE))
				{
					// remove the player from the index if the player is found 
					detected.RemoveAt(i);
				}
			}
			return detected;
		}
	}

	#endregion

	#region Unity References 

	private void Awake()
	{
		if (!m_SteeringSensor) m_SteeringSensor = GetComponent<SteeringRig>();
		if (!m_Animator) m_Animator = GetComponent<Animator>();
		if (!m_Rigidbody) m_Rigidbody = GetComponent<Rigidbody>();
		if (!m_SoldierWeapon) m_SoldierWeapon = GetComponentInChildren<SoldierWeapon>();
		m_SoldierMovement.Setup(this);

		if (!attributes)
		{
			GameDebug.Log($"Soldier {gameObject.transform.name} doesn't have the {this.gameObject} script attached!");
			return;
		}
	}

	private void Start()
	{
		if (m_StateRoutine != null)
			StopCoroutine(m_StateRoutine);

		m_StateRoutine = StartCoroutine(DefaultSearchState());
	}

	private void FixedUpdate()
	{
		if (!attributes || !m_Rigidbody)
			return;

		m_SoldierMovement.UpdateSoldierMovement(attributes, Time.fixedDeltaTime);
	}

	#endregion

	#region AI States 

	IEnumerator DefaultSearchState()
	{
		Start:

		if (SpottedTargets.Count > 0)
		{
			if (m_StateRoutine != null)
			{ 
				StopCoroutine(m_StateRoutine);
			}
			m_StateRoutine = StartCoroutine(AttackState());
			yield break;
		}
		else
		{
			float countdown = 1.0f;
			while (countdown > 0.0f)
			{
				var nextTargetDirection = m_SteeringSensor.GetSteeredDirection(-transform.position);
				MovementVelocity = nextTargetDirection;
				FacingDirection = nextTargetDirection;
				countdown -= Time.deltaTime;
				yield return null;
			}
		}

		goto Start;
	}

	IEnumerator AttackState(GameObject target = null)
	{
		// Check the targets list, if there are no targets, switch to default search state 
		if (target == null)
		{
			var targets = SpottedTargets;
			if (targets.Count == 0)
			{
				if (m_StateRoutine != null)
				{
					StopCoroutine(m_StateRoutine);
				}
				m_StateRoutine = StartCoroutine(DefaultSearchState());
				yield break;
			}
			target = targets[0].gameObject;
		}

		var cooldownAttackTimer = Random.Range(attributes.attackCooldownThreshold.x, attributes.attackCooldownThreshold.y);

		// find the distance between the target and this transform's current position (within an aggressive distance) 
		if ((target.transform.position - transform.position).magnitude > attributes.aggressiveDistance) // 
		{
			while (cooldownAttackTimer > 0)
			{
				if (target == null) break;
				var normalizedTargetDirection = (target.transform.position - transform.position).normalized;
				MovementVelocity = m_SteeringSensor.GetSteeredDirection(normalizedTargetDirection);
				FacingDirection = normalizedTargetDirection;

				AttachedWeapon.FireWeapon();
				if (AttachedWeapon.IsEmptyWeapon)
				{
					AttachedWeapon.Reload();
				}
				cooldownAttackTimer -= Time.deltaTime;
				yield return null;
			}
		}
		else
		{
			var strafingDirection = new Vector3
			{
				x = Random.Range(-1.0f, 1.0f),
				y = 0,
				z = Random.Range(-1.0f, 1.0f)
			}
			.normalized;

			while (cooldownAttackTimer > 0f)
			{
				if (target == null) break;

				var normalizedDirection = (target.transform.position - transform.position).normalized;
				MovementVelocity = m_SteeringSensor.GetSteeredDirection(strafingDirection);
				FacingDirection = normalizedDirection;

				AttachedWeapon.FireWeapon();
				if (AttachedWeapon.IsEmptyWeapon)
				{
					AttachedWeapon.Reload();
				}
				cooldownAttackTimer -= Time.deltaTime;
				yield return null;
			}
		}

		if (m_StateRoutine != null)
		{
			StopCoroutine(m_StateRoutine);
		}
		StartCoroutine(DefaultSearchState());
	}

	#endregion
}
