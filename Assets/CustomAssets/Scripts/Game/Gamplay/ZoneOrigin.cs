using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
///		Temporary - Could replace this with just a standard enum property value, instead of making a 
///		whole other class for this. Future ideas
/// </summary>
public class ZoneOrigin : MonoBehaviour
{
	public enum Zone
	{
		RED_ZONE,
		BLUE_ZONE,
		YELLOW_ZONE,
		NONE
	}

	[SerializeField] private Zone origin = Zone.RED_ZONE;

	public Zone GetZoneOrigin
	{ 
		get => origin;
		private set => origin = value;
	}
}
