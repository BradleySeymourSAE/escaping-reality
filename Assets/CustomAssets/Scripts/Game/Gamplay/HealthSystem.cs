﻿#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;
using UnityEngine.UI;
#endregion


[RequireComponent(typeof(AudioSource))]
public class HealthSystem : MonoBehaviour
{
	private const string HEALTH_SETTINGS = "Health Settings";
	private const string HEALTH_BAR = "Health Bar UI";
	private const string AUDIO = "Audio";
	[SerializeField] protected float m_CurrentHealth;

	[BoxGroup(HEALTH_SETTINGS)] [SerializeField] protected float maximumHealth = 100.0f;
	[BoxGroup(HEALTH_SETTINGS)] [SerializeField] protected GameObject m_DeathEffectPrefab;
	[BoxGroup(HEALTH_SETTINGS)] [SerializeField] protected GameObject m_HealthGainedEffectPrefab;

	[BoxGroup(HEALTH_BAR)] [SerializeField] protected Image m_HealthBarImage;
	[BoxGroup(HEALTH_BAR)] [SerializeField] protected Color CriticalColor = Color.red;
	[BoxGroup(HEALTH_BAR)] [SerializeField] protected Color FullHealthColor = Color.green;

	[BoxGroup(AUDIO)] [SerializeField] protected SoundFX m_DeathSound;
	[BoxGroup(AUDIO)] [SerializeField] protected SoundFX m_HurtSound;
	[HideInInspector] protected AudioSource m_AudioSource;

	[HideInInspector] public UnityEvent<float> onDamageReceived = new UnityEvent<float>();
	[HideInInspector] public UnityEvent<float> onHealthIncreased = new UnityEvent<float>();
	
	[SerializeField] protected TransformEvent onDeathEvent;
	private bool allowRagdollDeath;

	#region Getters / Setters 

	public float Health
	{
		get => m_CurrentHealth;
		protected set
		{
			m_CurrentHealth = Mathf.Clamp(value, 0, maximumHealth);

			if (m_HealthBarImage != null)
			{ 
				m_HealthBarImage.fillAmount = Mathf.Clamp01(m_CurrentHealth / maximumHealth);
				if (m_CurrentHealth > 50.0f && m_CurrentHealth <= maximumHealth)
				{
					m_HealthBarImage.color = Color.Lerp(m_HealthBarImage.color, FullHealthColor, Time.deltaTime);
				}
				else if (m_CurrentHealth >= 0 && m_CurrentHealth < 50.0f)
				{
					m_HealthBarImage.color = Color.Lerp(m_HealthBarImage.color, CriticalColor, Time.deltaTime);
				}
			}
		}
	}

	#endregion

	#region Unity References 

	private void Start()
	{
		Health = maximumHealth;
		if (!m_HealthBarImage) m_HealthBarImage = Utility.FindChild(gameObject, "health").GetComponent<Image>();
		m_AudioSource = Utility.Find<AudioSource>(gameObject);
		m_HealthBarImage.fillAmount = 1;
		allowRagdollDeath = GetComponent<AdvancedRagdollController>() ? true : false;
	}

	private void OnEnable()
	{
		onHealthIncreased.AddListener((amount) => HealthReceived(amount));
		onDamageReceived.AddListener((healthChange) => DamageReceived(healthChange));
	}

	private void OnDisable()
	{
		onHealthIncreased.RemoveListener((amount) => HealthReceived(amount));
		onDamageReceived.RemoveListener((healthChange) => DamageReceived(healthChange));
	}

	#endregion

	#region Private Methods 

	[Button("Test Health", buttonSize: ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(1, 0.5f, 0.5f, 0.9f)]
	protected virtual void TestHealth(int amount, bool add = false)
	{
		if (add)
			Health += amount;
		else
			Health -= amount;
	}

	protected virtual void HealthReceived(float amount) => Health += amount;

	protected virtual void DamageReceived(float amount)
	{
		Health -= amount;

		if (m_HurtSound != null)
			m_HurtSound.PlaySound();
		

		if (Health <= 0)
		{
			if (m_DeathEffectPrefab != null)
			{ 
				HandleDeath();
			}
		}
	}

	protected virtual void HandleDeath()
	{
		HandleDeathEffects(); // play death effects 
		if (onDeathEvent) onDeathEvent.Raise(transform); // raise the on death event, to be handled by the score system 
		if (allowRagdollDeath) // if allowRagdollDeath is true, will disable all of the rb components and do cool things 
			ActivateRagdoll(); // activates ragdoll physics 
		else 
			gameObject.SetActive(false); // otherwise, it's a normal object and we just want to disable it (not destroy it) 
	}

	public virtual void ActivateRagdoll()
	{
		var advancedRagdoll = GetComponent<AdvancedRagdollController>();
		if (advancedRagdoll)
			advancedRagdoll.SetActiveRagdoll(false);
	}

	protected virtual void HandleDeathEffects()
	{
		if (m_DeathEffectPrefab != null)
		{ 
			var deathEffect = Instantiate(m_DeathEffectPrefab, transform.position, Quaternion.identity) as GameObject;
			if (deathEffect)
				Destroy(deathEffect, 3.0f);
		}
		if (m_DeathSound)
			m_DeathSound.PlaySound();
	}

	#endregion

}