﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[RequireComponent(typeof(AudioSource))]
public class SoldierWeapon : MonoBehaviour 
{
	public enum WeaponFireMode { Automatic, Single }

	private const string SETTINGS = "General Settings";
	private const string WEAPON_SETTINGS = "Weapon Settings";
	private const string AUDIO_SETTINGS = "Audio";
	private const string WEAPON_EFFECTS = "Weapon Effects";
	private const string DEBUGGING = "debugging";

	[BoxGroup(WEAPON_SETTINGS)] [SerializeField] protected string WeaponName;
	[BoxGroup(WEAPON_SETTINGS)]	[SerializeField] protected bool unlocked = true;

	[BoxGroup(AUDIO_SETTINGS)] [SerializeField] protected SoundFX m_WeaponFireSound;
	[BoxGroup(AUDIO_SETTINGS)] [SerializeField] protected SoundFX m_WeaponReloadSound;

	[BoxGroup(WEAPON_EFFECTS)] [SerializeField] protected GameObject m_BulletCasing;
	[BoxGroup(WEAPON_EFFECTS)] [SerializeField] protected GameObject m_BulletPrefab;
	[BoxGroup(WEAPON_EFFECTS)] [SerializeField] protected GameObject m_MuzzleEffect;
	[BoxGroup(WEAPON_EFFECTS)] [SerializeField] protected Transform m_FiringPoint;
	[BoxGroup(WEAPON_EFFECTS)] [SerializeField] protected Transform m_WeaponSlider;

	[BoxGroup(SETTINGS)] [SerializeField] protected float m_Accuracy = 0.2f;
	[BoxGroup(SETTINGS)] [SerializeField] protected float m_BulletSpreadPerShot = 1.0f;
	[BoxGroup(SETTINGS)] [SerializeField] protected WeaponFireMode FireMode = WeaponFireMode.Automatic;
	[BoxGroup(SETTINGS)] [SerializeField] protected int m_TotalAmmunitionPerMagazine = 10;
	[BoxGroup(SETTINGS)] [SerializeField] protected int m_TotalAmmunition = 150;
	[BoxGroup(SETTINGS)] [SerializeField] protected int m_AmmunitionRemainingInClip = 10;
	[BoxGroup(SETTINGS)] [SerializeField] protected float m_MaximumFireRate = 0.3f;
	[BoxGroup(SETTINGS)] [SerializeField] protected float m_ReloadWaitTime = 1.5f;

	[SerializeField] private bool debugging;
	[ShowIf(DEBUGGING)] [SerializeField] protected AudioSource m_AudioSource;
	[ShowIf(DEBUGGING)] [SerializeField] protected bool canShoot;
	[ShowIf(DEBUGGING)] [SerializeField] protected bool hasShotWeapon;
	[ShowIf(DEBUGGING)] [SerializeField] protected float m_ShotErrorProbability;
	[ShowIf(DEBUGGING)] [SerializeField] protected float m_CurrentFireRateToShoot;

	Coroutine m_ReloadRoutine;

	#region Getters / Setters 

	public bool IsEmptyWeapon => m_TotalAmmunition <= 0;

	#endregion

	#region Unity References 

	protected virtual void Start()
	{
		m_CurrentFireRateToShoot = m_MaximumFireRate;
	}

	protected virtual void Update()
	{
		WeaponControl();
		
		if (debugging && m_FiringPoint != null)
		{
			RaycastHit information; 
			if (Physics.Raycast(m_FiringPoint.position, m_FiringPoint.forward, out information, 300.0f))
			{
				if (information.collider.attachedRigidbody)
				{
					var origin = m_FiringPoint.position;
					var point = information.point;
					Debug.DrawLine(origin, point, Color.green);
				}
			}
		}
	}

	#endregion

	#region Protected Methods 

	protected virtual void WeaponControl()
	{
		if (canShoot == false)
		{
			m_CurrentFireRateToShoot += Time.deltaTime;
			if (m_CurrentFireRateToShoot >= m_MaximumFireRate)
			{
				m_CurrentFireRateToShoot = 0;
				canShoot = true;
				hasShotWeapon = false;
			}
		}
		if (m_AmmunitionRemainingInClip == 0)
		{
			canShoot = false;
			m_CurrentFireRateToShoot = 0;
		}
		m_ShotErrorProbability = Mathf.Lerp(m_ShotErrorProbability, 0, m_Accuracy * Time.deltaTime);
	}

	protected virtual void ShootWeapon()
	{
		RaycastHit l_HitInformation;
		var l_BulletRotationPrecision = m_FiringPoint.forward;

		l_BulletRotationPrecision.x += Random.Range(-m_ShotErrorProbability / 2, m_ShotErrorProbability / 2);
		l_BulletRotationPrecision.y += Random.Range(-m_ShotErrorProbability / 2, m_ShotErrorProbability / 2);
		l_BulletRotationPrecision.z += Random.Range(-m_ShotErrorProbability / 2, m_ShotErrorProbability / 2);

		if (Physics.Raycast(m_FiringPoint.position, l_BulletRotationPrecision, out l_HitInformation, 1000.0f))
		{
			m_FiringPoint.LookAt(l_HitInformation.point);
			Debug.DrawLine(m_FiringPoint.position + m_FiringPoint.forward, l_HitInformation.point, Color.red);

			if (l_HitInformation.collider.gameObject != null)
			{
				m_ShotErrorProbability += m_BulletSpreadPerShot;
			}
			else
			{
				m_FiringPoint.rotation = transform.rotation;
				m_ShotErrorProbability += m_BulletSpreadPerShot;
			}
			// instantiate a new projectile 
			var bullet = Instantiate(m_BulletPrefab, m_FiringPoint.position, m_FiringPoint.rotation) as GameObject;
			bullet.GetComponent<Bullet>().FinalPoint = l_HitInformation.point;
			bullet.GetComponent<Bullet>().DestroyBulletRotation = l_HitInformation.normal;
			Destroy(bullet, 10.0f);
			if (FireMode.Equals(WeaponFireMode.Automatic) || FireMode.Equals(WeaponFireMode.Single))
			{
				EmitBulletCasing();
			}
		}
		hasShotWeapon = true; // update shooting weapon state 
		m_FiringPoint.rotation = transform.rotation; // reset shoot direction 
		var l_MuzzleFlashEffect = Instantiate(m_MuzzleEffect, m_FiringPoint.position, m_FiringPoint.rotation, transform) as GameObject; // instantiate muzzle flash effect 
		Destroy(l_MuzzleFlashEffect, 2.0f);

		m_CurrentFireRateToShoot = 0; // reset fire rate 
		canShoot = false; // reset can shoot 
		m_AmmunitionRemainingInClip--; // subtract 1 from the amount of bullets remaining 

		if (m_AudioSource && m_WeaponFireSound != null)
		{
			var audioManager = FindObjectOfType<SoundManager>();
			if (audioManager)
			{
				audioManager.PlaySound(m_WeaponFireSound, false, m_AudioSource);
			}
		}

		if (m_AmmunitionRemainingInClip <= 0)
			Reload();
	}

	protected virtual IEnumerator WaitForReload()
	{
		yield return new WaitForSeconds(m_ReloadWaitTime);
		canShoot = true;
		yield return null;
	}

	protected virtual void EmitBulletCasing()
	{
		if (m_BulletCasing)
		{
			var bulletCase = Instantiate(m_BulletCasing, m_WeaponSlider.position, transform.rotation) as GameObject;
			bulletCase.hideFlags = HideFlags.HideInHierarchy;
			Destroy(bulletCase, 5.0f);
		}
	}

	#endregion

	#region Public Methods 

	public virtual void FireWeapon()
	{
		if (canShoot)
		{
			ShootWeapon();
			hasShotWeapon = true;
		}
	}

	public virtual void Reload()
	{
		if (m_AmmunitionRemainingInClip < m_TotalAmmunitionPerMagazine) // if the soldier has actually shot their weapon they can reload 
		{
			if (m_TotalAmmunition >= m_TotalAmmunitionPerMagazine) // if the total amount of bullets is greater than or equal to bullets in magazine (150/10) 
			{
				m_AmmunitionRemainingInClip = m_TotalAmmunitionPerMagazine; // ammunition per clip = total ammunition per magazine 
				m_TotalAmmunition -= m_TotalAmmunitionPerMagazine; // deduct total ammunition per magazine from total ammunition 
			}
			else
			{
				m_AmmunitionRemainingInClip = m_TotalAmmunition; // otherwise, ammo remaining = total ammunition 
				m_TotalAmmunition = 0; // set total ammunition to 0 
			}

			if (m_ReloadRoutine != null)
				StopCoroutine(m_ReloadRoutine);

			m_ReloadRoutine = StartCoroutine(WaitForReload());
		}

		var audioManager = FindObjectOfType<SoundManager>();
		if (audioManager)
		{
			audioManager.PlaySound(m_WeaponReloadSound, false, m_AudioSource);
		}
	}

	#endregion
}
