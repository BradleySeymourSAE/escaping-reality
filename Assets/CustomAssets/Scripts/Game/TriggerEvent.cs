using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class TriggerEvent : MonoBehaviour
{
	[TagSelector] [SerializeField] private string target;
	[SerializeField] private GameObject m_CollectedEffect;
	[SerializeField] private SoundFX m_CollectedSound;
	[SerializeField] private Vector2 valueRange;

	public enum TriggerEventType {  Health, Ammunition }
	[SerializeField] private TriggerEventType type = TriggerEventType.Health;

	[SerializeField] private bool dontDestroy;


	private void PlayTriggerEffects()
	{
		if (m_CollectedEffect)
		{
			var effect = Instantiate(m_CollectedEffect, transform.position, Quaternion.identity) as GameObject;
			Destroy(effect, 1.5f);
		}
		if (m_CollectedSound)
		{
			m_CollectedSound.PlaySound();
		}
	}

	private void DestroyEffect()
	{
		if (dontDestroy)
		{
			return;
		}
		Destroy(gameObject);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag.Contains(target))
		{
			var otherObject = other.gameObject;

			if (type.Equals(TriggerEventType.Health))
			{
				if (otherObject.GetComponent<HealthSystem>())
				{
					var randomValue = Random.Range(valueRange.x, valueRange.y);
					otherObject.GetComponent<HealthSystem>().onHealthIncreased?.Invoke(randomValue);
				}
			}
			else if (type.Equals(TriggerEventType.Ammunition))
			{
				if (Utility.Has<Weapon>(otherObject))
				{ 
					var weaponSystem = Utility.Find<Weapon>(otherObject, true);

					if (weaponSystem)
					{ 
						var randomValue = Mathf.RoundToInt(Random.Range(valueRange.x, valueRange.y));
						weaponSystem.onAmmunitionChanged?.Invoke(randomValue);
					}
				}
			}
			PlayTriggerEffects();
			DestroyEffect();
		}
	}
}
