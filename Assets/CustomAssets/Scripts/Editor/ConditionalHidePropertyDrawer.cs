#region Namespaces
using UnityEngine;
using UnityEditor;
#endregion


[CustomPropertyDrawer(typeof(ConditionalHideAttribute))]
public class ConditionalHidePropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect p_RectPosition, SerializedProperty p_SerializedProperty, GUIContent p_GUIContentLabel)
    {
        var l_ConditionalHideAttribute = (ConditionalHideAttribute)attribute;
        var l_IsEnabled = GetConditionalHideAttributeResult(l_ConditionalHideAttribute, p_SerializedProperty);
        var l_WasGUIEnabled = GUI.enabled;

        GUI.enabled = l_IsEnabled;
        if (!l_ConditionalHideAttribute.HideInInspector || l_IsEnabled)
        {
            EditorGUI.PropertyField(p_RectPosition, p_SerializedProperty, p_GUIContentLabel, true);
        }

        GUI.enabled = l_WasGUIEnabled;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var l_ConditionallyHideAttribute = (ConditionalHideAttribute)attribute;
        var l_Enabled = GetConditionalHideAttributeResult(l_ConditionallyHideAttribute, property);

        if (!l_ConditionallyHideAttribute.HideInInspector || l_Enabled)
        {
            return EditorGUI.GetPropertyHeight(property, label);
        }
        else
        {
            //The property is not being drawn
            //We want to undo the spacing added before and after the property
            return -EditorGUIUtility.standardVerticalSpacing;
        }
    }

    private bool GetConditionalHideAttributeResult(ConditionalHideAttribute p_ConditionallyHiddenAttribute, SerializedProperty p_SerializedProperty)
    {
        var l_Enabled = true;

        var l_PropertyPath = p_SerializedProperty.propertyPath; //returns the property path of the property we want to apply the attribute to
        string l_ConditionalPath;

        if (!string.IsNullOrEmpty(p_ConditionallyHiddenAttribute.ConditionalSourceField))
        {
            l_ConditionalPath = l_PropertyPath.Replace(p_SerializedProperty.name, p_ConditionallyHiddenAttribute.ConditionalSourceField); 
            var l_SourcePropertyValue = p_SerializedProperty.serializedObject.FindProperty(l_ConditionalPath);

            if (l_SourcePropertyValue != null)
            {
                l_Enabled = CheckPropertyType(l_SourcePropertyValue);
            }
            else
            {
                //Debug.LogWarning("Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: " + condHAtt.ConditionalSourceField);
            }
        }

        if (!string.IsNullOrEmpty(p_ConditionallyHiddenAttribute.ConditionalSourceField2))
        {
            l_ConditionalPath =l_PropertyPath.Replace(p_SerializedProperty.name, p_ConditionallyHiddenAttribute.ConditionalSourceField2); //changes the path to the conditionalsource property path
            var l_SecondSourcePropertyValue = p_SerializedProperty.serializedObject.FindProperty(l_ConditionalPath);

            if (l_SecondSourcePropertyValue != null)
            {
                l_Enabled = l_Enabled && CheckPropertyType(l_SecondSourcePropertyValue);
            }
            else
            {
                //Debug.LogWarning("Attempting to use a ConditionalHideAttribute but no matching SourcePropertyValue found in object: " + condHAtt.ConditionalSourceField);
            }
        }

        if (p_ConditionallyHiddenAttribute.Inverse) l_Enabled = !l_Enabled;

        return l_Enabled;
    }

    private bool CheckPropertyType(SerializedProperty sourcePropertyValue)
    {
        switch (sourcePropertyValue.propertyType)
        {
            case SerializedPropertyType.Boolean:
                return sourcePropertyValue.boolValue;
            case SerializedPropertyType.ObjectReference:
                return sourcePropertyValue.objectReferenceValue != null;
            default:
                Debug.LogError("Data type of the property used for conditional hiding [" + sourcePropertyValue.propertyType + "] is currently not supported");
            return true;
        }
    }
}