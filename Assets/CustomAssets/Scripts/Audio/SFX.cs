﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[System.Serializable]
public class SFX
{
    [LabelText("SFX Type")]
    [LabelWidth(100)]
    [OnValueChanged("SFXChange")]
    [InlineButton("PlaySFX")]
    public SoundManager.SFXType soundType = SoundManager.SFXType.UI;

    [LabelText("$sfxLabel")]
    [LabelWidth(100)]
    [ValueDropdown("SFXType")]
    [OnValueChanged("SFXChange")]
    [InlineButton("SelectSFX")]
    public SoundFX sound;
    #pragma warning disable
    private string soundLabel = "SFX";
    #pragma warning disable
    [SerializeField] private bool showSettings = false;

    [ShowIf("showSettings")] [SerializeField]
    private bool editSettings = false;

    [InlineEditor(InlineEditorObjectFieldModes.Hidden)] [ShowIf("showSettings")] [EnableIf("editSettings")] [SerializeField]
    private SoundFX m_SoundFXBase;

    [Title("Audio Source")]
    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    [SerializeField]
    private bool waitToPlay = true;

    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    [SerializeField]
    private bool useDefault = true;

    [DisableIf("useDefault")]
    [ShowIf("showSettings")]
    [EnableIf("editSettings")]
    [SerializeField]
    private AudioSource defaultAudioSource;

    private void OnSoundFXChanged()
    {
        soundLabel = soundType.ToString() + " SFX";
        m_SoundFXBase = sound;
    }

    #if UNITY_EDITOR
    private void OnSoundFXSelected()
    {
        UnityEditor.Selection.activeObject = sound;
    }
    #endif

    /// <summary>
    ///     Get's list of SFX from manager, used in the inspector
    /// </summary>
    private List<SoundFX> GetSoundEffectType()
    {
        List<SoundFX> l_SoundsList;

        switch (soundType)
        {
            case SoundManager.SFXType.UI:
                l_SoundsList = SoundManager.instance.uiSounds;
                break;
            case SoundManager.SFXType.Ambient:
                l_SoundsList = SoundManager.instance.ambientSounds;
                break;
            case SoundManager.SFXType.Weapons:
                l_SoundsList = SoundManager.instance.weaponSounds;
                break;
            default:
                l_SoundsList = SoundManager.instance.uiSounds;
                break;
        }
        return l_SoundsList;
    }

   
    public void PlaySoundEffect()
    {
        if (useDefault || defaultAudioSource == null)
            SoundManager.PlaySFX(sound, waitToPlay, null);
        else
            SoundManager.PlaySFX(sound, waitToPlay, defaultAudioSource);
    }
}
