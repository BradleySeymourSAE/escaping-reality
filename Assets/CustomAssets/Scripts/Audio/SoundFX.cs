﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[InlineEditor]
[CreateAssetMenu(menuName = "New Sound Clip", fileName = "Gameplay/Audio/New Sound")]
public class SoundFX : SerializedScriptableObject
{ 
  
    [Space]
    [Title("Audio Clip")]
    [Required]
    public AudioClip sound;

    [Title("Clip Settings")]
    [Range(0f, 1f)]
    public float volume = 1f;
    [Range(0f, 0.2f)]
    public float volumeVariation = 0.05f;
    [Range(0f, 2f)]
    public float pitch = 1f;
    [Range(0f, 0.2f)]
    public float pitchVariation = 0.05f;

    [HideInInspector] private AudioSource m_ActiveSource = null;


    [Button("Play", buttonSize: ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(1.0f, 0.5f, 0.5f, 1)]
    public void PlaySound()
	{
        var manager = FindObjectOfType<SoundManager>();
        if (sound && manager != null)
		{
            m_ActiveSource = manager.GetComponent<AudioSource>();
            SoundManager.PlaySFX(this, false, m_ActiveSource);
		}
	}

    [ShowIf("m_ActiveSource")] [EnableIf("m_ActiveSource")] [Button("Stop", buttonSize: ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(0f, 0.5f, 0.5f)]
    public void StopSound()
	{
        if (m_ActiveSource != null)
             m_ActiveSource.Stop();
        else
            SoundManager.instance.GetComponent<AudioSource>().Stop();
	}
}
