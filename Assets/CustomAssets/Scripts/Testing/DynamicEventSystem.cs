using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CMF;
using Sirenix.OdinInspector;

[RequireComponent(typeof(AudioSource))]
public class DynamicEventSystem : TriggerArea
{

	[SerializeField] protected DynamicEventConfigSO[] m_DynamicEventConfigs;
	[SerializeField] protected TagCollection m_AllowColliderTags;
	[HideInInspector] private AudioSource m_AudioSource;
	private bool consumed;
	
	/// <summary>
	///		Returns a random dynamic event type 
	/// </summary>
	/// <returns></returns>
	private DynamicEventConfigSO ShuffleDynamicEvent() => Utility.RandomElement(m_DynamicEventConfigs);

	[SerializeField] private List<Transform> m_FocusedWalls = new List<Transform>();

	protected virtual void Awake()
	{
		if (!Utility.Has<AudioSource>(gameObject))
		{
			m_AudioSource = gameObject.AddComponent<AudioSource>();
		}
		else
		{
			if (!m_AudioSource) m_AudioSource = GetComponent<AudioSource>();
		}
	}

	private void StartDynamicEvent()
	{
		consumed = true;
		var chosenEvent = ShuffleDynamicEvent();
		if (chosenEvent && !chosenEvent.eventType.Equals(DynamicEventType.NONE))
		{
			// then we have chosen an event, might wan't to log it to the console to check 
			var currentEventType = chosenEvent.eventType;
			GameDebug.Log($"Current Event Type: {currentEventType}");
			switch (currentEventType)
			{
				case DynamicEventType.SOLDIER_REINFORCEMENTS:
					{ 
						
					}
					break;
				case DynamicEventType.MOVE_WALLS:
					{ 
						var proceduralWallPlacement = FindObjectOfType<ProceduralWallPlacementSystem>();
						m_FocusedWalls.Clear(); // clear the focsued list of walls 

						if (proceduralWallPlacement && proceduralWallPlacement.SpawnedWalls != null)
						{
							var spawnedWallCount = proceduralWallPlacement.SpawnedWalls.Count;
							for (int i = 0; i < chosenEvent.amountOfWallsToModify; i++)
							{
								var rand = new System.Random(
									System.DateTime.Now.ToString()
									.GetHashCode())
									.Next(spawnedWallCount);
								
								var selectedWall = proceduralWallPlacement.SpawnedWalls[rand];
								m_FocusedWalls.Add(selectedWall);
							}
						}
					}
					break;
				case DynamicEventType.ROTATE_WALLS:
					{ 
						 
					}
					break;
			}
		}

		/* if (consumed)
			Destroy(gameObject);
		*/
	}


	protected override void OnTriggerEnter(Collider collider)
	{
		if (collider.attachedRigidbody && m_AllowColliderTags.Includes(collider.attachedRigidbody.gameObject.tag))
		{
			if (consumed)
				return;

			rigidbodiesInTriggerArea.Add(collider.attachedRigidbody);
				
			GameDebug.Log($"Attached rigidbody contains tag! {collider.attachedRigidbody.gameObject.tag}");
			StartDynamicEvent();
		}
	}
}
