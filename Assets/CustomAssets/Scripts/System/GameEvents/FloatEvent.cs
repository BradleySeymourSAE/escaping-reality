﻿#region Namespaces
using UnityEngine;
#endregion


[CreateAssetMenu(fileName = "New Float Event", menuName = "Game Events/Float Event")]
public class FloatEvent : BaseGameEvent<float> { }