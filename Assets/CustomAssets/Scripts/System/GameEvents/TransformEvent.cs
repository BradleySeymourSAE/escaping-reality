﻿#region Namespaces
using UnityEngine;
#endregion

[CreateAssetMenu(fileName = "New Transform Event", menuName = "Game Events/Transform")]
public class TransformEvent : BaseGameEvent<Transform> { }
