#region Namespaces
using UnityEngine;
#endregion

[CreateAssetMenu(fileName = "New Vector Event", menuName = "Game Events/Vector 3")]
public class VectorEvent : BaseGameEvent<Vector3> { }
