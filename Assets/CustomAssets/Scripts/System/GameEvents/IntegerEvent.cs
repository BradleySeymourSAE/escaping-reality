﻿#region Namespaces
using UnityEngine;
#endregion


[CreateAssetMenu(fileName = "New Integer Event", menuName = "Game Events/Integer Event")]
public class IntegerEvent : BaseGameEvent<int>{ }