﻿#region Namespaces
using UnityEngine;
#endregion


[CreateAssetMenu(fileName = "New Sound Effect Event", menuName = "Game Events/Sound FX Event")]
public class SoundFXEvent : BaseGameEvent<SoundFX> { }