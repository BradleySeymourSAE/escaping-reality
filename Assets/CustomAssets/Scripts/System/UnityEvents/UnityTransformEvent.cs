﻿#region Namespaces
using UnityEngine;
using UnityEngine.Events;
#endregion

[System.Serializable] public class UnityTransformEvent : UnityEvent<Transform> { }