﻿#region Namespaces
using UnityEngine.Events;
#endregion

[System.Serializable] public class UnityIntegerEvent : UnityEvent<int> { }