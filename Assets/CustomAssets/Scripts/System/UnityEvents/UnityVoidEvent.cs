﻿#region Namespaces
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
#endregion

[System.Serializable]
public class UnityVoidEvent : UnityEvent<Void> { }