﻿#region Namespaces
using UnityEngine.Events;
#endregion

[System.Serializable] public class UnityFloatEvent : UnityEvent<float> { }