﻿#region Namespaces
using UnityEngine;
#endregion

public interface IEventListener<T> { void RaiseEvent(T parameter); }
