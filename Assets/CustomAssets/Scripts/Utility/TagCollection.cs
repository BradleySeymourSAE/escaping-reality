﻿#region Namespaces
using System.Collections.Generic;
using UnityEngine;
#endregion

[System.Serializable]
public class TagCollection
{
	[TagSelector] [SerializeField] protected List<string> tags = new List<string>();
	[SerializeField] protected bool m_EmptyTagsMeansIncludeAll = true;

	public bool Includes(string tag) => (tags.Count == 0 && m_EmptyTagsMeansIncludeAll) || tags.IndexOf(tag) != -1;

	public void Add(string tag) => tags.Add(tag);

	public void Remove(string tag) => tags.Remove(tag);
}