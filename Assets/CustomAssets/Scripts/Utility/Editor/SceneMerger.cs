﻿#if UNITY_EDITOR
#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using Sirenix.OdinInspector;
#endregion

[CreateAssetMenu(fileName = "Scene Merger", menuName = "Tools/Scene Merger", order = 1)]
public class SceneMerger : SerializedScriptableObject
{ 
	[SerializeField] private List<SceneAsset> m_GameScenes = new List<SceneAsset>();
	[SerializeField][ReadOnly] private string m_DefaultSceneName = "merged_scene";
	private List<Scene> m_AllAdditiveScenesOpen = new List<Scene>();


	[HorizontalGroup("Split", 0.5f)]
	[Button("Open Preview", ButtonSizes.Large), GUIColor(0, 1, 1), PropertyTooltip("Open's a preview of the merged scenes")]
	public void Preview()
	{
		for (int i = m_GameScenes.Count - 1; i >= 0; --i)
		{
			if (EditorSceneManager.GetActiveScene().name != m_GameScenes[i].name)
			{
				m_AllAdditiveScenesOpen.Add(EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(m_GameScenes[i]), OpenSceneMode.Additive));
			}
		}
	}

	[VerticalGroup("Split/right")]
	[Button("Close Preview", ButtonSizes.Large), GUIColor(0, 1, 0), PropertyTooltip("Closes the preview scene")]
	public void ClosePreview()
	{
		for (int i = m_AllAdditiveScenesOpen.Count - 1; i >= 0; --i)
		{
			EditorSceneManager.CloseScene(m_AllAdditiveScenesOpen[i], true);
		}
		m_AllAdditiveScenesOpen.Clear();
	}

	[TitleGroup("Scenes", alignment: TitleAlignments.Centered, horizontalLine: true, boldTitle: true, indent: true)]
	[Button("Merge Scenes", ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(1, 0, 0), PropertyTooltip("Begin's Scene Merge")]
	public void MergeScenes()
	{
		ClosePreview();
		var l_InstantiatedScene = EditorSceneManager.NewScene(NewSceneSetup.DefaultGameObjects);
		string l_SceneAssetPathname = AssetDatabase.GenerateUniqueAssetPath($"{Application.dataPath}/Scenes/{m_DefaultSceneName}.unity");
		GameDebug.Log($"Generating unique scene asset at path: {Application.dataPath}/Scenes/{m_DefaultSceneName}.unity");
		for (int i = m_GameScenes.Count - 1; i >= 0; --i)
		{
			Scene l_SceneOpened = EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(m_GameScenes[i]), OpenSceneMode.Additive);
			EditorSceneManager.MergeScenes(l_SceneOpened, l_InstantiatedScene);
		}
		EditorSceneManager.SaveScene(l_InstantiatedScene, l_SceneAssetPathname, false);
		AssetDatabase.Refresh();
		Selection.activeObject = this;
	}
}
#endif