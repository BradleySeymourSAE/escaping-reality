﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CopyTargetPosition : MonoBehaviour
{ 

	[SerializeField] private Transform m_Target;
	[SerializeField] private Vector3 m_OffsetPosition;


	private void Update()
	{
		if (m_Target)
		{
			transform.position = new Vector3(
				m_Target.position.x + m_OffsetPosition.x,
				0 + m_OffsetPosition.y, 
				m_Target.position.z + m_OffsetPosition.z
			);
		}
	}
}
