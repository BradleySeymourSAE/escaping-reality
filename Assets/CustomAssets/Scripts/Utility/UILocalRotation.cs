﻿#region Namespaces
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#endregion

public class UILocalRotation : MonoBehaviour
{
	[SerializeField] private Transform target;

	private void Update()
	{
		if (target != null)
		{
			transform.LookAt(target);
		}
	}
}