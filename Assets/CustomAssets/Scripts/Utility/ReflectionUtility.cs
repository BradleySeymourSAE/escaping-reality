﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

public static class ReflectionUtility
{

    public static object CallMethod(string p_TypeName, string p_MethodName)
    {
        return Type.GetType(p_TypeName).GetMethod(p_MethodName).Invoke(null, null);
    }
    public static object GetField(string typeName, string fieldName)
    {
        FieldInfo fieldInfo = Type.GetType(typeName).GetField(fieldName);
        return fieldInfo.GetValue(null);
    }
    public static Type GetNestedType(string typeName, string nestedTypeName)
    {
        return Type.GetType(typeName).GetNestedType(nestedTypeName);
    }

}
