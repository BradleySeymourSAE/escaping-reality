﻿#region Namespaces
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using Random = UnityEngine.Random;
using Object = UnityEngine.Object;
#endregion



public class Utility
{

	public static void SetupButtonActions(Button reference, UnityAction action, string text = "")
	{
		reference.onClick.RemoveAllListeners();
		reference.onClick.AddListener(action);
		
		if (Has<Text>(reference.gameObject))
			reference.gameObject.GetComponentInChildren<Text>().text = text;
		else if (Has<TMP_Text>(reference.gameObject))
			reference.gameObject.GetComponentInChildren<TMP_Text>().text = text;
	}

	public static void OpenWebpage(string path) => Application.OpenURL(path);

	#pragma warning disable
	public static bool Has<T>(GameObject p_ObjectToCheck) => p_ObjectToCheck.GetComponent<T>() != null || p_ObjectToCheck.GetComponentInChildren<T>() != null;

	public static T RandomElement<T>(T[] array) => array[Random.Range(0, array.Length)];

	public static T[] Push<T>(T[] array, T add)
	{
		T[] l_NewArray = new T[array.Length + 1];
		for (int index = 0; index < array.Length; index++)
		{
			l_NewArray[index] = array[index];
		}
		l_NewArray[array.Length] = add;
		return l_NewArray;
	}

	public static List<T> Push<T>(List<T> list, T index)
	{
		List<T> temp = new List<T>();
		temp.Clear();
		temp.Add(index);
		for (int i = 0; i < list.Count; i++)
			temp.Add(list[i]);
		return temp;
	}

	public static void Shuffle<T>(T[] arr, int iterations)
	{
		for (int i = 0; i < iterations; i++)
		{
			int rnd = Random.Range(0, arr.Length);
			T tmp = arr[rnd];
			arr[rnd] = arr[0];
			arr[0] = tmp;
		}
	}

	public static void Shuffle<T>(List<T> list, int iterations)
	{
		for (int i = 0; i < iterations; i++)
		{
			int rnd = Random.Range(0, list.Count);
			T tmp = list[rnd];
			list[rnd] = list[0];
			list[0] = tmp;
		}
	}

	public static T[] Unique<T>(T[] arr)
	{
		List<T> list = new List<T>();
		foreach (T t in arr)
		{
			if (!list.Contains(t))
			{
				list.Add(t);
			}
		}
		return list.ToArray();
	}

	public static List<T> Unique<T>(List<T> arr)
	{
		List<T> list = new List<T>();
		foreach (T t in arr)
		{
			if (!list.Contains(t))
			{
				list.Add(t);
			}
		}
		return list;
	}

	public static T Assign<T>(GameObject p_ObjectReference) where T : Component
	{
		if (Utility.Has<T>(p_ObjectReference))
		{
			if (p_ObjectReference.TryGetComponent<T>(out T value))
			{
				return value;
			}
		}
		return null;
	}

	public class JSON 
	{ 
		[System.Serializable]
		private class JSONDictionary
		{
			public List<string> keys = new List<string>();
			public List<string> values = new List<string>();
		}

		/// <summary>
		///		Takes a dictionary key value pairs and converts it into a string 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="dictionary"></param>
		/// <returns></returns>
		public static string Stringify<TKey, TValue>(Dictionary<TKey, TValue> dictionary)
		{
			CustomDictionary<string, string> temp = new CustomDictionary<string, string>();
			foreach (TKey key in dictionary.Keys)
			{
				temp.Add(JsonUtility.ToJson(key), JsonUtility.ToJson(dictionary[key]));
			}
			string response = JsonUtility.ToJson(temp);
			return response;
		}

		/// <summary>
		///		Takes a string and converts it into Javascript Object Notation (JSON) 
		/// </summary>
		/// <typeparam name="TKey"></typeparam>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="convertStringToJson"></param>
		/// <returns></returns>
		public static Dictionary<TKey, TValue> Parse<TKey, TValue>(string convertStringToJson)
		{
			JSONDictionary dictionary = JsonUtility.FromJson<JSONDictionary>(convertStringToJson);
			Dictionary<TKey, TValue> ret = new Dictionary<TKey, TValue>();
			for (int i = 0; i < dictionary.keys.Count; i++)
			{
				TKey key = JsonUtility.FromJson<TKey>(dictionary.keys[i]);
				TValue value = JsonUtility.FromJson<TValue>(dictionary.values[i]);
				ret[key] = value;
			}
			return ret;
		}
	
	}

	public static Texture2D CreateTexture(Color[] map, int Width, int Height)
	{
		Texture2D newTexture = new Texture2D(Width, Height);
		newTexture.filterMode = FilterMode.Point;
		newTexture.wrapMode = TextureWrapMode.Clamp; 
		newTexture.SetPixels(map);  
		newTexture.Apply();
		return newTexture;
	}

	public static Texture2D CreateTextureFromHeightMap(HeightMap p_TextureHeightMap)
	{
		int width = p_TextureHeightMap.values.GetLength(0);
		int height = p_TextureHeightMap.values.GetLength(1);

		Color[] l_ColorMap = new Color[width * height];

		for (int y = 0; y < height; y++)
		{
			// Loop through the width values 
			for (int x = 0; x < width; x++)
			{
				// Create a new texture color map using an inverse lerp function
				l_ColorMap[y * width + x] = Color.Lerp(
					Color.black, 
					Color.white, 
					Mathf.InverseLerp(p_TextureHeightMap.min, p_TextureHeightMap.max, p_TextureHeightMap.values[x, y])
				);
			}
		}
		return CreateTexture(l_ColorMap, width, height);
	}

	#region Math 

	public const int defaultSortingOrder = 5000;

	public static float GetFloatArrayAverage(float[] values, float sum = 0)
	{
		for (int i = 0; i < values.Length; i++)
		{
			sum += values[i];
		}
		return sum / values.Length;
	}

	public static float SignedAngleXZ(Vector3 angle, Vector3 secondAngle)
	{
		var l_FirstAngle = Mathf.Atan2(angle.x, angle.z) * Mathf.Rad2Deg;
		var l_SecondAngle = Mathf.Atan2(secondAngle.x, secondAngle.z) * Mathf.Rad2Deg;
		return Mathf.DeltaAngle(l_FirstAngle, l_SecondAngle);
	}

	/// <summary>
	///		Multiply each axis of a vector3 by a value 
	/// </summary>
	/// <param name="vector">The vector3 you want to multiply</param>
	/// <param name="value">The value you want to multiply the vector's angles by</param>
	/// <returns></returns>
	public static Vector3 MultiplyVector(Vector3 vector, float value)
	{
		return new Vector3
		{
			x = vector.x * value,
			y = vector.y * value,
			z = vector.z * value
		};
	}

	public static Vector3 RandomizeXY(Vector2 MinMaxValue)
	{
		return new Vector3
		{
			x = Random.Range(MinMaxValue.x, MinMaxValue.y),
			y = Random.Range(MinMaxValue.x, MinMaxValue.y),
			z = 0
		};
	}

	public static Vector3 RandomizeValues(Vector3 MinMaxValue)
	{
		return new Vector3
		{
			x = Random.Range(MinMaxValue.x, MinMaxValue.y),
			y = Random.Range(MinMaxValue.x, MinMaxValue.y),
			z = Random.Range(MinMaxValue.x, MinMaxValue.y)
		};
	}

	public static Vector3 GetRandomPositionWithinRectangle(float xMin, float xMax, float yMin, float yMax)
	{
		return new Vector3(UnityEngine.Random.Range(xMin, xMax), UnityEngine.Random.Range(yMin, yMax));
	}

	public static Vector3 GetRandomPositionWithinRectangle(Vector3 lowerLeft, Vector3 upperRight)
	{
		return new Vector3(UnityEngine.Random.Range(lowerLeft.x, upperRight.x), UnityEngine.Random.Range(lowerLeft.y, upperRight.y));
	}

	#region Get Direction From Angle 

	public static Vector3 GetDirectionFromAngle(int angle)
	{
		// angle = 0 -> 360
		float angleRad = angle * (Mathf.PI / 180f);
		return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
	}

	public static Vector3 GetDirectionFromAngle(float angle)
	{
		// angle = 0 -> 360
		float angleRad = angle * (Mathf.PI / 180f);
		return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
	}

	public static Vector3 GetDirectionFromAngleInt(int angle)
	{
		// angle = 0 -> 360
		float angleRad = angle * (Mathf.PI / 180f);
		return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
	}

	public static float GetAngleFromDirectionFloat(Vector3 dir)
	{
		dir = dir.normalized;
		float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		if (n < 0) n += 360;

		return n;
	}

	public static float GetAngleFromDirectionFloatXZ(Vector3 dir)
	{
		dir = dir.normalized;
		float n = Mathf.Atan2(dir.z, dir.x) * Mathf.Rad2Deg;
		if (n < 0) n += 360;

		return n;
	}

	public static int GetAngleFromDirection(Vector3 dir)
	{
		dir = dir.normalized;
		float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		if (n < 0) n += 360;
		int angle = Mathf.RoundToInt(n);

		return angle;
	}

	public static int GetAngleFromDirection180(Vector3 dir)
	{
		dir = dir.normalized;
		float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		int angle = Mathf.RoundToInt(n);

		return angle;
	}

	public static Vector3 ApplyRotationToDirection(Vector3 direction, Vector3 p_RotationDirection)
	{
		return ApplyRotationToDirection(direction, GetAngleFromDirectionFloat(p_RotationDirection));
	}

	/// <summary>
	///		Applies a rotation on the z axis (Forward) 
	/// </summary>
	/// <param name="direction"></param>
	/// <param name="angle"></param>
	/// <returns></returns>
	public static Vector3 ApplyRotationToDirection(Vector3 direction, float angle)
	{
		return Quaternion.Euler(0, 0, angle) * direction;
	}

	/// <summary>
	///		Applies a rotation on the Y axis 
	/// </summary>
	/// <param name="direction"></param>
	/// <param name="angle"></param>
	/// <returns></returns>
	public static Vector3 ApplyRotationToDirectionXZ(Vector3 direction, float angle)
	{
		return Quaternion.Euler(0, angle, 0) * direction;
	}

	#endregion

	#endregion

	#region Parse 

	// Parses a float value, return default if failed
	public static float ParseFloat(string input, float p_DefaultValue)
	{
		float f;
		if (!float.TryParse(input, out f))
		{
			f = p_DefaultValue;
		}
		return f;
	}

	// Parse an integer value, returns default if failed
	public static int ParseInt(string input, int p_DefaultValue)
	{
		int i;
		if (!int.TryParse(input, out i))
		{
			i = p_DefaultValue;
		}
		return i;
	}

	public static int ParseInt(string inputString) => ParseInt(inputString, -1);

	#endregion

	#region UI 

	// Create Text in the World
	public static TextMesh CreateWorldText(string text, Transform parent = null, Vector3 localPosition = default(Vector3), int fontSize = 40, Color? color = null, TextAnchor textAnchor = TextAnchor.UpperLeft, TextAlignment textAlignment = TextAlignment.Left, int sortingOrder = defaultSortingOrder)
	{
		if (color == null) color = Color.white;
		return CreateWorldText(parent, text, localPosition, fontSize, (Color)color, textAnchor, textAlignment, sortingOrder);
	}

	// Create Text in the World
	public static TextMesh CreateWorldText(Transform parent, string text, Vector3 localPosition, int fontSize, Color color, TextAnchor textAnchor, TextAlignment textAlignment, int sortingOrder)
	{
		GameObject gameObject = new GameObject("World_Text", typeof(TextMesh));
		Transform transform = gameObject.transform;
		transform.SetParent(parent, false);
		transform.localPosition = localPosition;
		TextMesh textMesh = gameObject.GetComponent<TextMesh>();
		textMesh.anchor = textAnchor;
		textMesh.alignment = textAlignment;
		textMesh.text = text;
		textMesh.fontSize = fontSize;
		textMesh.color = color;
		textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
		return textMesh;
	}

	#endregion

	#region Game Objects / Component 

	public static T Find<T>(GameObject p_GameObjectToCheck, bool isChild = false) where T : Component
	{
		if (Has<T>(p_GameObjectToCheck))
		{
			if (isChild)
			{
				return p_GameObjectToCheck.GetComponentInChildren<T>();
			}
			
			var component = p_GameObjectToCheck.GetComponent<T>() ? p_GameObjectToCheck.GetComponent<T>() : p_GameObjectToCheck.GetComponentInChildren<T>();
			return component;
		}
		else
		{
			GameDebug.Log($"Could not find that component in object {p_GameObjectToCheck.name}");
			return null;
		}
	}

	public static T Find<T>() where T : Component => Object.FindObjectOfType<T>() ? Object.FindObjectOfType<T>() : null;

	public static GameObject GetCurrentPlayer(string target = "Player") => GameObject.FindGameObjectWithTag(target);

	/// <summary>
	///		Set Game Object Name, Removing the clone tag from the object 
	/// </summary>
	/// <param name="p_GameObject">The game object to check </param>
	/// <param name="ignore">The tag to ignore, defaults to "(Clone)"</param>
	/// <param name="name">The name you want to assign to a game object</param>
	/// <returns></returns>
	public static string SetGameObjectName(GameObject p_GameObject, string ignore = "(Clone)", string name = null)
	{
		var l_Name = name ?? p_GameObject.name;
		if (l_Name.Contains(ignore))
			l_Name = l_Name.Replace(ignore, "");
		return l_Name;
	}

	#endregion

	#region Find Child Transform 

	public static Transform FindChild(GameObject p_GameObjectToSearch, string p_SearchByName = "")
	{
		return (
			from p_Transform in p_GameObjectToSearch.GetComponentsInChildren<Transform>()
			where p_Transform.gameObject.name == p_SearchByName
			select p_Transform.gameObject.transform
		).First();
	}


	public static List<Transform> GetChildObjects(Transform parent)
	{
		if (parent != null && parent.childCount > 0)
		{
			List<Transform> objects = new List<Transform>();
			objects.Clear();

			for (int i = 0; i < parent.childCount; i++)
			{
				var child = parent.GetChild(i);
				objects.Add(child);
			}

			return objects;
		}
		else
		{
			return null;
		}
	}

	public static string GetTimeHoursMinutesSeconds(float time, bool hours = true, bool minutes = true, bool seconds = true, bool milliseconds = true)
	{
		string h0, h1, m0, m1, s0, s1, ms0, ms1, ms2;
		GetTimeCharacterStrings(time, out h0, out h1, out m0, out m1, out s0, out s1, out ms0, out ms1, out ms2);
		string h = h0 + h1;
		string m = m0 + m1;
		string s = s0 + s1;
		string ms = ms0 + ms1 + ms2;

		if (hours)
		{
			if (minutes)
			{
				if (seconds)
				{
					if (milliseconds)
					{
						return h + ":" + m + ":" + s + "." + ms;
					}
					else
					{
						return h + ":" + m + ":" + s;
					}
				}
				else
				{
					return h + ":" + m;
				}
			}
			else
			{
				return h;
			}
		}
		else
		{
			if (minutes)
			{
				if (seconds)
				{
					if (milliseconds)
					{
						return m + ":" + s + "." + ms;
					}
					else
					{
						return m + ":" + s;
					}
				}
				else
				{
					return m;
				}
			}
			else
			{
				if (seconds)
				{
					if (milliseconds)
					{
						return s + "." + ms;
					}
					else
					{
						return s;
					}
				}
				else
				{
					return ms;
				}
			}
		}
	}

	public static void GetTimeCharacterStrings(float time, out string h0, out string h1, out string m0, out string m1, out string s0, out string s1, out string ms0, out string ms1, out string ms2)
	{
		int s = Mathf.FloorToInt(time);
		int m = Mathf.FloorToInt(s / 60f);
		int h = Mathf.FloorToInt((s / 60f) / 60f);
		s = s - m * 60;
		m = m - h * 60;
		int ms = (int)((time - Mathf.FloorToInt(time)) * 1000);

		if (h < 10)
		{
			h0 = "0";
			h1 = "" + h;
		}
		else
		{
			h0 = "" + Mathf.FloorToInt(h / 10f);
			h1 = "" + (h - Mathf.FloorToInt(h / 10f) * 10);
		}

		if (m < 10)
		{
			m0 = "0";
			m1 = "" + m;
		}
		else
		{
			m0 = "" + Mathf.FloorToInt(m / 10f);
			m1 = "" + (m - Mathf.FloorToInt(m / 10f) * 10);
		}

		if (s < 10)
		{
			s0 = "0";
			s1 = "" + s;
		}
		else
		{
			s0 = "" + Mathf.FloorToInt(s / 10f);
			s1 = "" + (s - Mathf.FloorToInt(s / 10f) * 10);
		}


		if (ms < 10)
		{
			ms0 = "0";
			ms1 = "0";
			ms2 = "" + ms;
		}
		else
		{
			// >= 10
			if (ms < 100)
			{
				ms0 = "0";
				ms1 = "" + Mathf.FloorToInt(ms / 10f);
				ms2 = "" + (ms - Mathf.FloorToInt(ms / 10f) * 10);
			}
			else
			{
				// >= 100
				int _i_ms0 = Mathf.FloorToInt(ms / 100f);
				int _i_ms1 = Mathf.FloorToInt(ms / 10f) - (_i_ms0 * 10);
				int _i_ms2 = ms - (_i_ms1 * 10) - (_i_ms0 * 100);
				ms0 = "" + _i_ms0;
				ms1 = "" + _i_ms1;
				ms2 = "" + _i_ms2;
			}
		}
	}
	#endregion

	#region Mouse Position 

	/// <summary>
	/// Get Mouse Position in World with Z = 0f
	/// </summary>
	public static Vector3 GetMouseWorldPosition()
	{
		Vector3 vec = GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
		vec.z = 0f;
		return vec;
	}

	public static Vector3 GetMouseWorldPositionWithZ() => GetMouseWorldPositionWithZ(Input.mousePosition, Camera.main);
	
	public static Vector3 GetMouseWorldPositionWithZ(Camera worldCamera) => GetMouseWorldPositionWithZ(Input.mousePosition, worldCamera);

	public static Vector3 GetMouseWorldPositionWithZ(Vector3 screenPosition, Camera p_WorldCamera)
	{
		Vector3 l_MousePosition = p_WorldCamera.ScreenToWorldPoint(screenPosition);
		return l_MousePosition;
	}

	public static Vector3 GetDirectionToMouse(Vector3 p_FromDirection)
	{
		Vector3 l_MousePosition = GetMouseWorldPosition();
		return (l_MousePosition - p_FromDirection).normalized;
	}

	#endregion

}


public static class ArrayExtensions
{
	public static int IndexOf<T>(this T[] array, T index)
	{
		for (var i = 0; i < array.Length; i++)
		{
			if (array[i].Equals(index))
				return i;
		}
		return -1;
	}
}

/// <summary>
///		Object for storing height map data 
/// </summary>
public struct HeightMap
{ 
	public readonly float[,] values;
	public readonly float min;
	public readonly float max;
	
	public HeightMap(float[,] values, float min, float max)
	{
		this.values = values;
		this.min = min;
		this.max = max;
	}
}