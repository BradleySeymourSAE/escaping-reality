﻿#region Namespaces
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#endregion

public class DebugDraw
{
    public static void DrawCircle(Vector3 center, float radius, Color color, float duration, int divisions)
    {
        for (int i = 0; i <= divisions; i++)
        {
            Vector3 vec1 = center + Utility.ApplyRotationToDirection(new Vector3(0, 1) * radius, (360f / divisions) * i);
            Vector3 vec2 = center + Utility.ApplyRotationToDirection(new Vector3(0, 1) * radius, (360f / divisions) * (i + 1));
            Debug.DrawLine(vec1, vec2, color, duration);
        }
    }

    public static void DrawRectangle(Vector3 minXY, Vector3 maxXY, Color color, float duration)
    {
        Debug.DrawLine(new Vector3(minXY.x, minXY.y), new Vector3(maxXY.x, minXY.y), color, duration);
        Debug.DrawLine(new Vector3(minXY.x, minXY.y), new Vector3(minXY.x, maxXY.y), color, duration);
        Debug.DrawLine(new Vector3(minXY.x, maxXY.y), new Vector3(maxXY.x, maxXY.y), color, duration);
        Debug.DrawLine(new Vector3(maxXY.x, minXY.y), new Vector3(maxXY.x, maxXY.y), color, duration);
    }
    public static void DrawLines(Vector3 position, Color color, float size, float duration, Vector3[] points)
    {
        for (int i = 0; i < points.Length - 1; i++)
        {
            Debug.DrawLine(position + points[i] * size, position + points[i + 1] * size, color, duration);
        }
    }

    public static void DrawLines(Vector3 position, Color color, float size, float duration, float[] points)
    {
        List<Vector3> vecList = new List<Vector3>();
        for (int i = 0; i < points.Length; i += 2)
        {
            Vector3 vec = new Vector3(points[i + 0], points[i + 1]);
            vecList.Add(vec);
        }
        DrawLines(position, color, size, duration, vecList.ToArray());
    }

}