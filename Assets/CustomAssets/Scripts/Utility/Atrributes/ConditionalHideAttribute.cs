﻿#region Namespaces
using UnityEngine;
using System;
#endregion



[AttributeUsage(AttributeTargets.Field)]
public class ConditionalHideAttribute : PropertyAttribute
{
    public readonly string ConditionalSourceField;
    public string ConditionalSourceField2 = "";
    public bool HideInInspector = true;
    public bool Inverse = false;

    public ConditionalHideAttribute(string p_Conditional, bool p_ShouldHideInInspector = true, bool p_InverseBooleanValue = false, string p_SecondConditional = "")
    { 
        ConditionalSourceField = p_Conditional;
        ConditionalSourceField2 = p_SecondConditional;
        HideInInspector = p_ShouldHideInInspector;
        Inverse = p_InverseBooleanValue;
    }
}