﻿#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
#endregion


[InlineEditor]
[CreateAssetMenu(fileName = "New Score System Configuration", menuName = "Gameplay/Score System")]
public class ScoreSystemConfigSO : SerializedScriptableObject
{
	[SerializeField] private FloatEvent testOnScoreChanged;
	public float scorePerSecond = 1.0f;
	public float bonusPoints = 20.0f;
	public float pointsPerRedSoldier = 1.0f;
	public float pointsPerYellowSoldier = 3.0f;
	public float pointsPerBlueSoldier = 5.0f;
	
	[Button("Test Score Changed", buttonSize: ButtonSizes.Large, Style = ButtonStyle.Box), GUIColor(1.0f, 0.5f, 0.5f)]
	private void TestScoreChanged(float value)
	{
		testOnScoreChanged.Raise(value);
	}
}