﻿#if UNITY_EDITOR
using Sirenix.OdinInspector;
using UnityEngine;


public abstract class Equippable : Item
{
    [BoxGroup(STATS_BOX_GROUP)]
    public float durability;

    [VerticalGroup(LEFT_VERTICAL_GROUP + "/Modifiers")]
    public StatList modifiers;
}
#endif
