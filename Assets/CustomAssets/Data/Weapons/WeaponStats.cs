﻿#if UNITY_EDITOR
using UnityEngine;
using Sirenix.OdinInspector;
using SensorToolkit;


    public class WeaponStats
    : Equippable
    {
        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseWeaponDamage;

        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseFireRate;

        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseReloadSpeed;

        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseAccuracy;

        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseBulletSpeed;

        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseLifetime;

        [BoxGroup(STATS_BOX_GROUP)]
        public float BaseBulletForce;
        
        [BoxGroup(STATS_BOX_GROUP)]
        public RaySensor.UpdateMode RaycastSensorUpdateMode;

        [BoxGroup(STATS_BOX_GROUP)]
        public SoundFX WeaponFireClip;

        public override ItemTypes[] SupportedItemTypes
        {
            get
            {
                return new ItemTypes[]
                {
                    ItemTypes.M1911,
                    ItemTypes.AK47
                };
            }
        }
}
#endif
