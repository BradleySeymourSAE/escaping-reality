﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


[CreateAssetMenu(fileName = "New Spawn Item Config", menuName = "Gameplay/Item Spawner")]
[InlineEditor]
public class SpawnItemConfigSO : SerializedScriptableObject
{
	private const string GENERAL = "Spawn Settings";
	private const string VFX = "/CustomAssets/Prefabs/VFX/";
	
	public bool isZoneArea;

	[BoxGroup(GENERAL)]
	public GameObject prefab;

	[BoxGroup(GENERAL)]
	[ShowIf("isZoneArea")]
	[AssetList(Path = VFX + "zones")]
	public GameObject zoneAreaEffect;

	[BoxGroup(GENERAL)]
	[ShowIf("isZoneArea")]
	[AssetList(Path = VFX + "consumables")]
	public GameObject zoneSpawnEffect;

	[BoxGroup(GENERAL)]
	public int spawnCount;

	[BoxGroup(GENERAL)]
	public float spawnRate;

	[BoxGroup(GENERAL)]
	public int startingSpawnCount;

	[BoxGroup(GENERAL)]
	public Vector3 area = Vector3.zero;

	[BoxGroup(GENERAL)]
	public float checkSphereRadius = 1.0f;

	[BoxGroup(GENERAL)]
	public LayerMask obstructingLayers;

}