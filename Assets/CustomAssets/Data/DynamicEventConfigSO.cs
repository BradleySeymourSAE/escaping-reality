﻿#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
#endregion

public enum DynamicEventType
{
	NONE = 0,
	SOLDIER_REINFORCEMENTS = 1,
	MOVE_WALLS = 2,
	ROTATE_WALLS = 3
}

[CreateAssetMenu(fileName = "New Dynamic Event Config", menuName = "Gameplay/Dynamic Event Config")]
public class DynamicEventConfigSO : SerializedScriptableObject
{
	private const string GENERAL = "General";
	private const string EVENT_TYPE = "eventType";
	
	[ShowIf(EVENT_TYPE, DynamicEventType.MOVE_WALLS)]
	public enum Direction { LEFT, RIGHT }


	[BoxGroup(GENERAL)]
	public DynamicEventType eventType = DynamicEventType.NONE;

	[BoxGroup(GENERAL)]
	public SoundFX eventStartedSound;

	[BoxGroup(GENERAL)]
	public float eventCooldownDuration;

	[ShowIf(EVENT_TYPE, DynamicEventType.SOLDIER_REINFORCEMENTS)]
	public Vector2 soldierSpawnAmount;

	[ShowIf(EVENT_TYPE, DynamicEventType.SOLDIER_REINFORCEMENTS)]
	public float spawnRateIncrement;

	[ShowIf(EVENT_TYPE, DynamicEventType.ROTATE_WALLS)]
	public float rotationSpeed = 360.0f;

	[ShowIf(EVENT_TYPE, DynamicEventType.ROTATE_WALLS)]
	public float rotationTime = 5.0f;

	[ShowIf(EVENT_TYPE, DynamicEventType.ROTATE_WALLS)]
	public float maximumRotationDegrees = 180.0f;

	[ShowIf(EVENT_TYPE, DynamicEventType.MOVE_WALLS)]
	public int amountOfWallsToModify = 6;

	[ShowIf(EVENT_TYPE, DynamicEventType.MOVE_WALLS)]
	public float movementThreshold = 10.0f;

	[ShowIf(EVENT_TYPE, DynamicEventType.MOVE_WALLS)]
	public float movementSpeed = 10.0f;

	[ShowIf(EVENT_TYPE, DynamicEventType.MOVE_WALLS)]
	public Direction moveDirecion = Direction.LEFT;

}
