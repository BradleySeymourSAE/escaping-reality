#region Namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using SensorToolkit;
#endregion

[InlineEditor]
[CreateAssetMenu(fileName = "Soldier Attributes", menuName = "Gameplay/AI/Soldier Behaviour Attributes")]
public class ZoneSoldierAIAttributes : SerializedScriptableObject
{ 
	public bool toggleSensors;
	public bool toggleMovementSettings;
	public bool toggleAnimationSettings;
	public bool enableItemPickup;
	public bool allowEditing;

	#pragma warning disable
	private float MaxSpeed = 1500.0f;
	private float MaxTurn = 500.0f;
	private float MaxStrafe = 500.0f;

	[TitleGroup("Sensors", AnimateVisibility = true, Alignment = TitleAlignments.Centered, HorizontalLine = true, BoldTitle = true)]
	[ShowIf("toggleSensors", true)]

	[TitleGroup("Sensors/Sight")] [ShowIf("toggleSensors", true)]
	public SensorMode sightDetectionMode = SensorMode.RigidBodies;
	
	[TitleGroup("Sensors/Sight")] [ShowIf("toggleSensors", true)]
	public float maxSightDistance = 20.0f;

	[TitleGroup("Behaviour", AnimateVisibility = true, Alignment = TitleAlignments.Centered, HorizontalLine = true, BoldTitle = true)]
	public float aggressiveDistance = 10.0f;
	
	[TitleGroup("Behaviour")]
	public Vector2 attackCooldownThreshold = new Vector2(0.1f, 5.0f);

	[TitleGroup("Movement", Alignment = TitleAlignments.Centered, HorizontalLine = true, BoldTitle = true)] [ShowIf("toggleMovementSettings", true)]
	[PropertyRange(0, "MaxSpeed"), GUIColor(0, 1, 1)] [ShowIf("toggleMovementSettings", true)]
	public float speed = 100.0f;
	
	[PropertyRange(0, "MaxTurn"), GUIColor(0, 1, 1)] [ShowIf("toggleMovementSettings", true)]
	public float turn = 250.0f;

	[PropertyRange(0, "MaxStrafe"), GUIColor(0, 1, 1)] [ShowIf("toggleMovementSettings", true)]
	public float strafe = 250.0f;

	[TitleGroup("Animation")]
	[ShowIf("toggleAnimationSettings", true)]
	[EnableIf("allowEditing", true)] 
	public string VelocityHash = "HorizontalSpeed";


}