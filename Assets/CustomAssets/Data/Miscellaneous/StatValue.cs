﻿#if UNITY_EDITOR
    using System;
    using UnityEngine;
    using Sirenix.OdinInspector;


    [Serializable]
    public struct StatValue : IEquatable<StatValue>
    {
        [HideInInspector]
        public StatModifierType Type;

        [Range(-100, 100)]
        [LabelWidth(70)]
        [LabelText("$Type")]
        public float Value;

        public StatValue(StatModifierType type, float value)
        {
            this.Type = type;
            this.Value = value;
        }

        public StatValue(StatModifierType type)
        {
            this.Type = type;
            this.Value = 0;
        }

        public bool Equals(StatValue other)
        {
            return this.Type == other.Type && this.Value == other.Value;
        }
    }
#endif
