﻿#if UNITY_EDITOR
public enum StatModifierType
{
    Speed,
    Accuracy
}
#endif